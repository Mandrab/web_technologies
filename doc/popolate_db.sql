INSERT INTO `foods`(`name`, `description`, `gluten_free`, `vegan`, `cost`, `active`, `id_food_type`, `id_restaurant`) VALUES 
("crostini da paul", "crostini misti con...", 0, 0, 3.00, 1, 1, 1),
("crostini vegan da paul", "crostini misti con...", 0, 1, 3.00, 0, 1, 1),
("cappelletti da paul", "cappelletti in brodo", 0, 1, 8.00, 1, 2, 1),
("tagliatelle da paul", "tagliatelle al ragu", 0, 0, 8.00, 1, 2, 1),
("gnocchi da paul", "gnocchi al ragù o al gorgonzola", 0, 0, 7.00, 0, 2, 1),
("castrato da paul", "castrato", 1, 0, 9.00, 0, 3, 1),
("tagliata da paul", "tagliata", 1, 0, 14.00, 1, 3, 1),
("castrato da paul", "castrato", 1, 0, 9.00, 0, 3, 1),
("tagliata da paul", "tagliata", 1, 0, 14.00, 1, 3, 1),
("tiramisù da paul", "dolce fin troppo calorico", 1, 0, 4.00, 0, 4, 1),
("fernet branca da paul", "tagliata", 1, 0, 2.00, 1, 5, 1),

("crostini da giulio", "crostini misti con...", 0, 0, 3.00, 0, 1, 2),
("crostini vegan da giulio", "crostini misti con...", 0, 1, 3.00, 0, 1, 2),
("cappelletti da giulio", "cappelletti in brodo", 0, 1, 8.00, 0, 2, 2),
("tagliatelle da giulio", "tagliatelle al ragu", 0, 0, 8.00, 0, 2, 2),
("gnocchi da giulio", "gnocchi al gorgonzola", 0, 0, 7.00, 1, 2, 2),
("castrato da giulio", "castrato", 1, 0, 8.00, 1, 3, 2),
("tagliata da giulio", "tagliata", 1, 0, 12.00, 0, 3, 2),

("crostini da sut", "crostini misti con...", 0, 0, 2.50, 0, 1, 3),
("crostini vegan da sut", "crostini misti con...", 0, 1, 2.50, 0, 1, 3),
("cappelletti da sut", "cappelletti in brodo", 0, 1, 6.00, 0, 2, 3),
("tagliatelle da sut", "tagliatelle al ragu", 0, 0, 5.00, 0, 2, 3),
("gnocchi da sut", "gnocchi al gorgonzola", 0, 0, 5.00, 1, 2, 3),
("castrato da sut", "castrato", 1, 0, 6.00, 1, 3, 3),
("tagliata da sut", "tagliata", 1, 0, 10.00, 0, 3, 3);

INSERT INTO `food_types`(`name`, `description`) VALUES 
("antipasto", "preparati e atti a stuzzicare l'appetito"),
("primo", "primo piatto"),
("secondo", "secondo piatto"),
("dolce", "cibo di chiusura del pasto"),
("digestivo", "alcolico di chiusura");

INSERT INTO `notifications` (`data`, `description`, `seen`, `id_order`, `id_restaurant`, `id_notification_type`, `id_receiver`) VALUES
('1999-01-01', 'new restaurant', 1, NULL, 1, 1, 1),
('1999-01-01', 'restaurant accepted', 1, NULL, 1, 3, 2),
('1999-01-01', 'new restaurant', 1, NULL, 2, 1, 1),
('1999-01-01', 'restaurant accepted', 1, NULL, 2, 3, 3),
('1999-01-01', 'new restaurant', 1, NULL, 3, 1, 1),
('1999-01-01', 'restaurant accepted', 1, NULL, 3, 3, 4),
('1999-01-01', 'new restaurant', 1, NULL, 3, 1, 1),
('1999-01-01', 'restaurant rejected', 1, NULL, 4, 3, 3),

('1999-01-01', 'new order', 1, 1, NULL, 6, 2),
('1999-01-01', 'order status changed', 1, 1, NULL, 7, 5),
('1999-01-01', 'order status changed', 1, 1, NULL, 7, 5),
('1999-01-01', 'new order', 1, 2, NULL, 6, 2),
('1999-01-01', 'order status changed', 1, 2, NULL, 7, 5),
('1999-01-01', 'order status changed', 1, 2, NULL, 7, 5),
('1999-01-01', 'new order', 1, 3, NULL, 6, 2),
('1999-01-01', 'order status changed', 1, 3, NULL, 7, 5),
('1999-01-01', 'order status changed', 1, 3, NULL, 7, 5),
('1999-01-01', 'new order', 1, 4, NULL, 6, 2),
('1999-01-01', 'order status changed', 1, 4, NULL, 7, 5),
('1999-01-01', 'order status changed', 1, 4, NULL, 7, 5),
('1999-01-01', 'new order', 1, 5, NULL, 6, 2),
('1999-01-01', 'order status changed', 1, 5, NULL, 7, 5),
('1999-01-01', 'order status changed', 1, 5, NULL, 7, 5),

('1999-01-01', 'new order', 1, 6, NULL, 6, 2),
('1999-01-01', 'order status changed', 1, 6, NULL, 7, 5),
('1999-01-01', 'new order', 1, 7, NULL, 6, 2),
('1999-01-01', 'order status changed', 1, 7, NULL, 7, 5),
('1999-01-01', 'new order', 1, 8, NULL, 6, 2),
('1999-01-01', 'order status changed', 1, 8, NULL, 7, 5),
('1999-01-01', 'new order', 1, 9, NULL, 6, 2),
('1999-01-01', 'order status changed', 1, 9, NULL, 7, 5);

INSERT INTO `notification_types`(`name`, `description`) VALUES 
("new_restaurant","A restaurant has been added"),
("restaurant_rejected","A restaurant has been rejected"),
("restaurant_accepted","A restaurant has been accepted"),
("restaurant_locked","A restaurant has been locked"),
("restaurant_unlocked","A restaurant has been unlocked"),
("new_order","New order incomming"),
("new_order_status","An order has changed is status"),
("seller locked","A lock caused by seller lock");

INSERT INTO `openings`(`week_day`, `opening_hour`, `closing_hour`, `id_restaurant`) VALUES 
("1","12:00", "15:00", 1),
("1","19:00", "23:00", 1),
("2","19:00", "23:00", 1),
("2","19:00", "23:00", 1),
("4","19:00", "23:00", 1),
("5","19:00", "23:00", 1),
("7","12:00", "15:00", 1),

("1","12:00", "15:00", 2),
("1","19:00", "23:00", 2),
("2","19:00", "23:00", 2),

("2","19:00", "23:00", 3),
("4","19:00", "23:00", 3),
("5","19:00", "23:00", 3),

("7","12:00", "15:00", 4);

INSERT INTO `orders`(`hour`, `status`, `delivery_place`, `id_client`, `id_delivery_man`) VALUES 
("19:00", "complete", "via università 1", 0, 1),
("18:00", "complete", "via università 1", 0, 2),
("22:00", "complete", "via università 1", 1, 3),
("12:00", "complete", "via università 1", 0, 4),
("13:00", "complete", "via università 1", 1, 5),
("12:00", "new", "via università 1", 1, 6),
("13:00", "new", "via università 1", 1, 7),
("21:00", "new", "via università 1", 1, 8),
("23:00", "new", "via università 1", 1, 9);

INSERT INTO `order_foods`(`id_food`, `id_order`) VALUES 
(1, 1), (2, 1), 
(1, 2),
(12, 3), (13, 3), (14, 3),
(1, 4), (2, 4), 
(21, 5), (22, 5), 
(1, 6), (2, 6), 
(1, 7),
(12, 8), (13, 8), (14, 8),
(1, 9), (2, 9), 
(21, 10), (22, 10);


INSERT INTO `restaurants` (`name`, `address`, `tel`, `p_iva`, `photo_link`, `description`, `active`, `del_predicted_time`, `id_owner`, `id_restaurant_type`) VALUES 
("Da Paul", "via salvo 7", "777", "xxxxxxxxx", "/res/img/restaurants/no_image.svg", "Poca roba ma buona", 1, "00:20", 2, 3),
("Da Giulio", "via giuseppe mazzini 16", "888", "yyyyyyyyy", "/res/img/restaurants/no_image.svg", "Come a casa vostra", 1, "00:15", 3, 1),
("Da Sut", "via emporio armani 1", "999", "zzzzzzzzz", "/res/img/restaurants/no_image.svg", "Buoni piatti al giusto prezzo", 1, "00:15", 4, 2),
("Da Jonny", "via cesare battisti 9", "000", "abcdefghi", "/res/img/restaurants/no_image.svg", "Piadina e crescioni", 0, "00:5", 3, 4);

INSERT INTO `restaurant_types`(`name`, `description`) VALUES 
("Agriturismo","Cucina tipica locale"),
("Ristorante","Cucina di buona qualità con vari tipi di cibi"),
("Ristorante stellato","Cucina di ottima qualità con vari tipi di cibi"),
("Fast food","Pranzi veloci");

INSERT INTO `users` (`name`, `surname`, `tel`, `email`, `password`, `active`, `user_type`) VALUES
('admin', 'superuser', '0000000000', 'root@mail.com', 'root', 1, 'admin'),
('Paolo', 'Baldini', '0000000002', 'paul@mail.com', 'pwd', 1, 'seller'),
('Andrea', 'Giulianini', '0000000003', 'giulio@mail.com', 'pwd', 1, 'seller'),
('Lorenzo', 'Sutera', '0000000004', 'sut@mail.com', 'pwd', 0, 'seller'),
('Alessandro', 'Lom', '0000000001', 'user1@mail.com', 'pwd', 1, 'user'),
('Marco', 'Mel', '0000000005', 'user2@mail.com', 'pwd', 0, 'user');

INSERT INTO `votes`( `val`, `id_restaurant`) VALUES 
(5, 1), (2, 1), (3, 1), (4, 1), (5, 1), 
(5, 2), (5, 2), 
(3, 3);
