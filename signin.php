<?php

    session_start();

    if ( isset( $_SESSION['user_type'] ) ) {

        header( 'Location: /src/server/management/' . $_SESSION['user_type'] . '/home.php' );
        exit;

    }

    require $_SERVER['DOCUMENT_ROOT'] . '/src/client/unlogged/signin/signin.php';
