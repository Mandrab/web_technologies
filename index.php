<?php

    session_start();

    if ( !isset( $_SESSION['user_type'] ) ) {                  // if not logged -> go to login page
        header("Location: login.php");
        exit;
    } else {                                                // if logged -> go to home
        header("Location: src/server/management/" 
            . $_SESSION['user_type'] . "/home.php");
        exit;
    }