<?php
    $page_name = 'Restaurant';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/header.php'
?>

    </header>

    <script src="/src/client/logged/user/see_cart.js"></script>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12 text-center">
                <h2 id="page_title"></h2>
            </div>
        </div>

        <div class="row">
            <div class="col-12 text-center">
                <h1> Sta ordinando da <?php echo $res[0]['name'] ?></h1>
            </div>
            <div class="col-12 text-center">
                <span>Indirizzo: <?php echo $res[0]['address'] ?></span>
            </div>
            <div class="col-12 text-center">
                <span>Orario consegna:</span>
                <select id="hour" name="hour">
                    <?php foreach($valid_hour as $valid){ ?>
                        <option value="<?php echo $valid ?>"><?php echo substr($valid, 0, -3); ?></option>
                    <?php } ?>
                </select>
            </div>
            <?php foreach($array_prod as $prod){ ?>
            <div class="col-12 bg-grey p-2 mt-3 mb-5">
                <div class="row text-center">
                    <div class="col-md-6 offset-md-2">
                    <?php echo $prod[0]['name'] ?>
                    </div>
                    <div class="col-md-2">
                    Totale: <?php echo $prod['qt']*$prod[0]['cost'] ?>€
                    </div>
                    <div class="col-md-2">
                    Quantità <?php echo $prod['qt'] ?>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="col-12 mb-5">
                <div class="row">
                    <div class="col-md-4 offset-md-2">
                        <button class="confirm form-control btn btn-green">Conferma Ordine</button>
                    </div>
                    <div class="col-md-4">
                        <button class="delete form-control btn btn-red">Pulisci Carrello</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/footer.php'
?>
