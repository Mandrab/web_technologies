<?php 
    $page_name = 'User_Home';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/header.php'
?>

    </header>

    <?php
        require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/check_permission.php';
        checkAuth( array( 'user' ) );
    ?>
    
    <script src="/src/client/logged/user/home.js"></script>
    
    <div class="container">
        <div class="row mt-3 text-center">
            <div class="col-12">
                <h3 id="page_title" class="black text-center"></h3>
            </div>
        </div>
        <div class="row mt-3 mb-5">
            <div class="col-12 col-md-6 mt-2">
                <button id="restaurant_list" class="form-control btn btn-green"></button>
            </div>
            
            <div class="col-12 col-md-6 mt-2">
                <button id="order_list" class="form-control btn btn-green"></button>
            </div>

            <div class="col-12 col-md-6 mt-2">
                <button id="notification_list" class="form-control btn btn-green"></button>
            </div>

            <div class="col-12 col-md-6 mt-2">
                <button id="logout" class="form-control btn btn-red"></button>
            </div>

        </div>
    </div>

<?php 
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/footer.php'
?>