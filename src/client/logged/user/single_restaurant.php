<?php
    $page_name = 'Restaurant';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/header.php'
?>

    </header>

    <script src="/src/client/logged/user/single_restaurant.js"></script>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12 text-center">
               <h2 id="page_title"></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center">
                <img src="<?php echo $restaurant[0]['photo'] ?>" class="img-fluid" alt="restaurant image">
            </div>
            <div class="col-12 text-center">
                <h1><?php echo $restaurant[0]['name'] ?></h1>
            </div>
            <div class="col-12 text-center">
                <span>Indirizzo: <?php echo $restaurant[0]['address'] ?></span>
            </div>
            <div class="col-12 text-center">
                <img src="/res/img/votes/<?php
                    if ( $restaurant[0]['active'] && $restaurant[0]['vote'] != '' )
                        echo ceiling( $restaurant[0]['vote'], 0.5 );
                    else
                        echo '0';
                ?>_star.svg" alt="vote stars">
            </div>
            <div class="col-12 text-center">
                <span>Descrizione: <?php echo $restaurant[0]['description'] ?></span>
            </div>
            <?php foreach($cat as $key=>$c){ ?>
                <div class="col-12">
                    <h2><?php echo ucfirst($key)?></h2>
                </div>
            <?php foreach($c as $f){ ?>
                <div class="col-12 bg-grey p-2 mt-3">
                    <form method="post" class="send-cart">
                        <input type="hidden" name="id_p" value="<?php echo $f[0] ?>">
                        <input type="hidden" name="id_r" value="<?php echo $id ?>">
                        <div class="row">
                            <div class="col-4 offset-2">
                                <p><?php echo ucfirst($f[1]) ?></p>
                                <p>Descrizione: <?php echo ucfirst($f[2]) ?></p>
                            </div>
                            <div class="col-3">
                                <p><?php echo $f['cost']?>€</p>
                            </div>
                            <div class="col-2">
                                <button type="submit" class="btn btn-green"><i class="fas fa-shopping-cart"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            <?php    }
            } ?>
        </div>
    </div>

<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/footer.php'
?>
