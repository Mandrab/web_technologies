<?php 

    if ( $_REQUEST['type'] == "order" ) { ?>
        <div class="col-12 p-1 mb-1 list_block">
            <div class="listblock row" type="order" val="<?php echo $row['id'] ?>">
                <div class="col-2 text-center">
                    <label id="name_r"><?php echo $row['name'] ?></label>
                </div>
                <div class="col-2 text-center">
                    <label id="status"><?php echo $row['status'] ?></label>
                </div>
                <div class="col-2 text-center">
                    <label id="hour"><?php echo $row['hour'] ?></label>
                </div>
                <div class="col-2 text-center">
                    <label id="del_place"><?php echo $row['delivery_place'] ?></label>
                </div>
                <div class="col-2 text-center">
                    <label id="tot_cost"><?php echo $row['cost'] ?></label>
                </div>
            </div>
        </div> <?php
    }