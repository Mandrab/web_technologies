<?php
    $page_name = 'Restaurant';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/header.php'
?>

    </header>

    <script src="/src/client/logged/user/all_restaurant.js"></script>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12 mt-3">
                <h3 id="page_title" class="text-center"></h3>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row mt-3">
            <div class="col-8 offset-2 offset-md-0 col-md-3">
                <form method="get">
                    <div class="row">
                        <input id="nameSe" name="name" type="text" class="form-control" placeholder="Trova il ristorante" value="<?php
                            if( isset( $_REQUEST['name'] ) ) 
                                echo $_REQUEST['name']; 
                        ?>">
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <p>Categorie: </p>
                        </div>
                        <div class="col-6">
                            <select name="type" id="typeSe" class="form-control">
                                <?php foreach($cat as $c) { ?>
                                    <option value="<?php echo $c['id']?>" <?php 
                                        if( isset( $_REQUEST['type'] ) && $c['id'] == $_REQUEST['type'] ) 
                                            echo 'selected';
                                    ?>><?php echo ucfirst($c['name']) ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <label for="dateSe">Per Oggi?</label>
                        </div>
                        <div class="col-6">
                            <input type="checkbox" name="date" id="dateSe" class="form-control" value="<?php echo date('N') ?>" <?php 
                                if( isset( $_REQUEST['date'] ) && $_REQUEST['date'] == 1 ) 
                                    echo 'checked'; 
                            ?>>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <p>Ora Consegna: </p>
                        </div>
                        <div class="col-6">
                            <select name="hour" id="hourSe" class="form-control">
                                <?php 
                                    for( $date = strtotime( '00:30:00' ); date('H:i:s', $date) != '00:00:00'; $date = strtotime("+30 minutes", $date) ) { ?>
                                        <option value="<?php echo date('H:i:s', $date) ?>" <?php if( isset( $_REQUEST['type'] ) && date('H:i:s', $date) == $_REQUEST['hour'] ) echo 'selected' ?>>
                                            <?php echo date('H:i:s', $date) ?>
                                        </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <button type="submit" id="search" class="form-control btn btn-green">Cerca!</button>
                    </div>
                </form>
            </div>
            <div class="col-12 col-md-9 mt-5">
                <div class="row">
                    <?php foreach($result as $res) { ?>
                    <div class="col-md-6 restaurant" val="<?php echo $res['id']?>">
                        <img src="<?php echo $res['photo'] ?>" class="img-fluid" alt="restaurant image">
                        <span><?php echo $res['name'] ?></span><br/>
                        <span>Indirizzo: <?php echo $res['address'] ?></span><br/>
                        <img src="/res/img/votes/<?php
                            if ( $res['active'] && $res['vote'] != '' )
                                echo ceiling( $res['vote'], 0.5 );
                            else
                                    echo '0';
                            ?>_star.svg" alt="vote stars"><br/>
                        <span>Numero di voti: <?php echo $res['n_votes'] ?></span><br/>
                        <span>Descrizione: <?php echo $res['description'] ?></span>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    
    <div >
        <button id="modify_button" type="submit"
        <?php if ( $_SESSION['user_type'] != 'seller' ) echo "hidden"; ?>>
        </button>
        <button id="delete_button" type="submit"
        <?php if ( $_SESSION['user_type'] != 'seller' ) echo "hidden"; ?>>
        </button>
        <button id="lock_button" type="submit"
        <?php if ( $_SESSION['user_type'] != 'admin' || !$result[0]['active'] ) echo "hidden"; ?>>
        </button>
        <button id="unlock_button" type="submit"
        <?php if ( $_SESSION['user_type'] != 'admin' || $result[0]['active'] ) echo "hidden"; ?>>
        </button>
    </div>

<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/footer.php'
?>
