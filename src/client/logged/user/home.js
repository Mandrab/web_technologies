$( document ).ready( function() {

    $( '#restaurant_list' ).click( function() {
        window.location.href = "/src/server/management/user/all_restaurant.php";
    } );

    $( "#order_list" ).click( function() {
        window.location.href = "/src/server/management/commons/list.php?type=order";
    } );

    $( "#notification_list" ).click( function() {
        window.location.href = "/src/server/management/commons/notifications.php";
    } );

    $( "#logout" ).click( function() {
        window.location.href = "/src/server/access/unlogger.php";
    } );

} );
