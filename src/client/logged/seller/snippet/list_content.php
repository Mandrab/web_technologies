<?php 

    if ( $_REQUEST['type'] == "foods" ) { ?>
        <div class="listblock" type="food" val="<?php echo $row['id'] ?>">
            <label id="name"><?php echo $row['name'] ?></label>
            <label id="cost"><?php echo $row['cost'] ?></label>
            <?php if( $row['gluten_free'] ) { ?>
                <label id="gluten_free"></label>
            <?php } ?>
            <?php if( $row['vegan'] ) { ?>
                <label id="vegan"></label>
            <?php } ?>
            <label id="active"><?php echo $row['active'] ? "Active" : "Banned" ?></label>
        </div> <?php
    }