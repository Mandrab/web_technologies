$( document ).ready( function() {

    $( '#restaurant_list' ).click( function() {
        window.location.href = "/src/server/management/commons/list.php?type=restaurants";
    } );

    $( "#order_list" ).click( function() {
        var d = new Date();

        //VARIABILE DI PROVA DATA FITTIZIA rimuovere se si vuole utilizzare l'ora attuale
        //d = new Date("July 21, 1983 14:15:00");

        // console.log(d.getHours()+":"+d.getMinutes()+":00");
        // console.log($("#idseller").attr("value"));
        // die();

        $.ajax({
            method: "POST",
            url: "/src/server/management/seller/orders_searcher.php",
            data: { idseller: $( "#idseller" ).attr("value"), hour: d.getHours()+":"+d.getMinutes()+":00"}
        }).done( function( res ) {
            console.log("done");
            console.log(res);
            if (res=="notfound") {
              $( "#notify" ).text("Non hai nessun ordine attualmente! ");
            }else if(res=="ok"){
              //vai alla pagina ordini passando l'id seller-> $("#idseller").attr("value")
              window.location.href = "/src/server/management/seller/orders_list.php?hour="+d.getHours()+":"+d.getMinutes()+":00";
              // window.location.href = "/src/server/management/seller/orders_list.php?idseller="+$("#idseller").attr("value");
            }
        });
        return false;
    } );

    $( "#notification_list" ).click( function() {
        window.location.href = "/src/server/management/commons/notifications.php";
    } );



    $( ".new" ).click( function() {
        event.stopPropagation();
        window.location.href = "/src/server/management/seller/add_restaurant.php";
    } );

    $( "#logout" ).click( function() {
        window.location.href = "/src/server/access/unlogger.php";
    } );

} );
