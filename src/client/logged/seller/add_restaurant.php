<?php
    $page_name = 'Modify Restaurant';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/header.php'
?>

    </header>

    <script src="/src/client/logged/seller/add_restaurant.js"></script>

    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2 id="restaurantTitle">Compila Ristorante</h2>
            </div>
        </div>
        <div class="row">
            <div id="elem_list" class="col-12">
                <form id="restaurant_data" <?php 
                    if ( isset( $_REQUEST['id'] ) )
                        echo 'hidden';
                ?> class="row">
                    <div class="col-md-6 col-12">
                        <label for="restaurant_name">Nome Locale:</label> <!-- TODO : move to xml-->
                    </div>
                    <div class="col-md-6 col-12">
                        <input type="text" name="restaurant_name" id="restaurant_name" class="form-control" value="Nome" required> <br>
                    </div>
                    <div class="col-md-6 col-12">
                        <label for="address">Indirizzo:</label> <!-- TODO : move to xml-->
                    </div>
                    <div class="col-md-6 col-12">
                        <input type="text" name="address" id="address" class="form-control" value="Indirizzo" required> <br>
                    </div>
                    <div class="col-md-6 col-12">
                        <label for="tel">Telefono:</label> <!-- TODO : move to xml-->
                    </div>
                    <div class="col-md-6 col-12">
                        <input type="text" name="tel" id="tel" class="form-control" value="Telefono" required> <br>
                    </div>
                    <div class="col-md-6 col-12">
                        <label for="types">Tipologia Locale:</label> <!-- TODO : move to xml-->
                    </div>
                    <div class="col-md-6 col-12">
                        <select id="types" name="types" class="form-control">
                            <?php $c = 1;
                            foreach ($result as $r) {?>
                            <option value="<?php echo $c++ ?>"><?php echo $r['name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-6 col-12">
                        <label for="des">Descrizione:</label> <!-- TODO : move to xml-->
                    </div>
                    <div class="col-md-6 col-12">
                        <textarea name="des" id="des" class="form-control" value="des">Descrizione</textarea> <br>
                    </div>
                    <div class="col-md-6 col-12">
                        <label for="iva">Partita IVA:</label> <!-- TODO : move to xml-->
                    </div>
                    <div class="col-md-6 col-12">
                        <input type="text" name="iva" id="iva" class="form-control" value="P. IVA" required> <br>
                    </div>
                    <div class="col-md-6 col-12">
                        <label for="d_time">Tempo di consegna previsto:</label> <!-- TODO : move to xml-->
                    </div>
                    <div class="col-md-6 col-12">
                        <input type="text" name="d_time" id="d_time" class="form-control" value="00:15" required> <br>
                    </div>
                    <div class="col-md-6 col-12">
                        <button id="button_confirm" type="submit" class="form-control btn btn-green">Aggiungi</button>
                    </div>
                </form>
                <form id="hours_table" <?php 
                    if ( isset( $_REQUEST['id'] ) )
                        echo 'val="' . $_REQUEST['id'] . '"';
                    else
                        echo 'hidden';
                ?> class="row">
                    <div class="col-md-6 col-12">
                        <select id="days" class="form-control">
                            <option value="1">Lunedì</option>
                            <option value="2">Martedì</option>
                            <option value="3">Mercoled'</option>
                            <option value="4">Gioved'</option>
                            <option value="5">Venerdì</option>
                            <option value="6">Sabato</option>
                            <option value="7">Domenica</option>
                        </select>
                    </div>
                    <div class="col-md-6 col-12">
                        <select id="open" class="form-control">
                            <option value="08:00:00">8:00</option>
                            <option value="08:30:00">8:30</option>
                            <option value="09:00:00">9:00</option>
                            <option value="09:30:00">9:30</option>
                            <option value="10:00:00">10:00</option>
                            <option value="10:30:00">10:30</option>
                            <option value="11:00:00">11:00</option>
                            <option value="11:30:00">11:30</option>
                            <option value="12:00:00">12:00</option>
                            <option value="12:30:00">12:30</option>
                            <option value="13:00:00">13:00</option>
                            <option value="13:30:00">13:30</option>
                            <option value="14:00:00">14:00</option>
                            <option value="14:30:00">14:30</option>
                            <option value="15:00:00">15:00</option>
                            <option value="15:30:00">15:30</option>
                            <option value="16:00:00">16:00</option>
                            <option value="16:30:00">16:30</option>
                        </select>
                    </div>
                    <div class="col-md-6 col-12">
                        <select id="close" class="form-control">
                            <option value="11:00:00">11:00</option>
                            <option value="11:30:00">11:30</option>
                            <option value="12:00:00">12:00</option>
                            <option value="12:30:00">12:30</option>
                            <option value="13:00:00">13:00</option>
                            <option value="13:30:00">13:30</option>
                            <option value="14:00:00">14:00</option>
                            <option value="14:30:00">14:30</option>
                            <option value="15:00:00">15:00</option>
                            <option value="15:30:00">15:30</option>
                            <option value="16:00:00">16:00</option>
                            <option value="16:30:00">16:30</option>
                            <option value="17:00:00">17:00</option>
                            <option value="17:30:00">17:30</option>
                            <option value="18:00:00">18:00</option>
                            <option value="18:30:00">18:30</option>
                            <option value="19:00:00">19:00</option>
                            <option value="19:30:00">19:30</option>
                            <option value="20:00:00">20:00</option>
                        </select>
                    </div>
                    <div class="col-md-6 col-12">
                        <input type="hidden" name="id_restaurant" id="id_restaurant" class="form-control" value="<?php echo $idrestaurant ?>" required>
                    </div>
                    <div class="col-md-6 col-12">
                        <table id="opening" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Giorno</th>
                                    <th>Apertura</th>
                                    <th>Ciusura</th>
                                </tr>
                            </thead>
                            <tbody id="opening_body">
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-4 offset-md-2 col-12">
                        <button id="add" type="button" class="form-control btn btn-green">Aggiungi apertura</button> <!-- TODO : aggiungere Azione-->
                    </div>
                    <div class="col-md-4 col-12">
                        <button id="button_clear" type="button" class="form-control btn btn-red">Pulisci tabella</button>
                    </div>
                    <div class="col-md-4 col-12">
                        <button id="confirm_openings" type="button" class="form-control btn btn-green">Conferma</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/footer.php'
?>
