<?php
    $page_name = 'Modify Menù';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/header.php'
?>
    <script src="/src/client/logged/seller/menu_mod.js"></script>

    </header>

    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2 id="restaurantTitle">Modifica Menù</h2>
            </div>
        </div>
        <div>
          <div id="elem_list">
              <?php
              $prec="";

                  foreach( $result as $row ) { ?>
                      <div class="listblock " type="food" value="<?php echo $row['id'] ?>">

                          <?php
                          if ($row['tipo'] != $prec) {
                              $prec=$row['tipo'];
                              ?>
                              <h3 class="col-12 text-center">
                                  <label id="type" class="category title"><?php echo $row['tipo'] ?></label> <br/>
                              </h3>
                              <?php
                          }?>
                          <div class="row mb-4 bg-light" >

                            <div class="col-md-5 col-sm-5 col-3">
                              <label id="namef"><?php echo $row['name'] ?></label>
                            </div>


                            <div class="col-md-2 col-sm-3 col-4 offset-md-2">
                              <button id="modifica" class="form-control btn btn-green mod"  type="button" name="button">modifica</button>
                            </div>
                            <div class="col-md-2 col-sm-3 col-4 ">
                              <button id="rimuovi" class="form-control btn btn-red del"  type="button" name="button">rimuovi</button>
                            </div>

                            <div class="col-2">
                              <label id="cost">€<?php echo $row['cost'] ?></label>
                            </div>
                            <div class="col-md-7 col-sm-7 col-6 ">
                              <label id="description"><?php echo $row['description'] ?></label>
                            </div>
                            <div class="col-md-3 col-sm-3 col-3 text-primary">
                            <?php if( $row['gluten_free'] ) { ?>

                                <small id="gluten_free">NoGlut </small>
                            <?php } ?>
                            <?php if( $row['vegan'] ) { ?>
                                <small id="vegan">Veg</small>

                            <?php } ?>
                            </div>

                            <input type="hidden" name="id_restaurant" id="id_restaurant" value="<?php echo $idrestaurant?>" required>


                        </div> <?php
                    }
                ?>
                <div class="offset-2 col-8 col-md-6 offset-md-3 mb-3">
                  <button id="new_food" class="form-control btn btn-green new"  type="button" name="button">Nuovo Piatto</button>
                </div>
              </div>
          </div>
        </div>
    </div>

<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/footer.php'
?>
