<?php
    $page_name = 'View Order';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/header.php'
?>

    </header>

    <script src="/src/client/logged/seller/orders_foods_list.js"></script>

    <div>
      <div class="row ">
          <div class="col-12 text-center">
              <h2 id="orderTitle">Elenco cibi ordine</h2>
          </div>
      </div>
        <div>
          <div id="elem_list" class="col text-center">

            <?php
                foreach( $result as $row ) { ?>
                  <div class="col-12 offset-0 offset-md-3 col-md-6 mb-3">

                        <div id="<?php echo $row['id_food'] ?>" val="<?php echo $row['id_food']?>" class="listblock row" type="order">
                          <div class="col-12 ">
                              <label class="form-control bg-light" id="namem"><?php echo $row['name'] ?>  €:<?php echo $row['cost'] ?>  Quantità: <?php echo $row['quantity'] ?></label>
                            </div>
                      </div>
                    </div><?php
                }
            ?>

            <div class="col-6 offset-3 offset-md-4 col-md-4 mb-3">
              <button class="form-control btn btn-green confirm" id="<?php echo $result[0]['id_order']?>" type="button" name="button">Eseguito</button>
            </div>
          </div>
        </div>
    </div>

<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/footer.php'
?>
