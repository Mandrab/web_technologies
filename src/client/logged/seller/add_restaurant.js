$( document ).ready( function() {

    var opening = [];

    $( "#restaurant_data" ).submit( function() {

        $.ajax( {
            method: "POST",
            url: "/src/server/management/seller/insert_restaurant.php",
            data: $( this ).serialize()
        } ).done( function( res ) {
            console.log(res);
            if ( res != 'err' )
                window.location.href = window.location.href + '?id=' + res;
            else
                window.location.reload();
        } );

        return false;
    } );

    $( "#add" ).click( function() {

        opening.push( { day: $( '#days' ).val(),
            open: $( '#open' ).val(),
            close: $( '#close' ).val()
        } );

        $( '#opening_body' ).append(
            "<tr id=\"tbody\">                   \
                <td>" + $( '#days' ).val() + "</td>     \
                <td>" + $( '#open' ).val() + "</td>   \
                <td>" + $( '#close' ).val() + "</td>    \
            </tr>" );

    });

    $( '#button_clear' ).click( function() {
        opening = [];
        $( '#opening tbody tr' ).remove();
    } );

    $( '#confirm_openings' ).click( function() {

        opening.forEach( e => {

            $.ajax( {
                    method: "POST",
                    url: "/src/server/management/seller/openings_inserter.php",
                    data: { id_restaurant: $( '#hours_table' ).attr( 'val' ),
                            open: e['open'],
                            close: e['close'],
                            days: e['day'] }
            } ).done( function(res) {
                console.log(res);
            } );

        } ); 
        window.location.href = '/src/server/management/user/home.php'; 

    } );

} );
