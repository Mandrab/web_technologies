<?php
    $page_name = 'Modify Meal';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/header.php'
?>
    <script src="/src/client/logged/seller/food_mod.js"></script>

    </header>

    <div>
      <div class="row">
          <div class="col-12 text-center">
              <h2 id="mealTitle">Compila Modifica</h2>
          </div>
      </div>
        <div>
            <div id="elem_list">

              <div class=" offset-1 col-10 text-center">
                  <form id="mod_data_food">
                    <div class="row" >
                      <div class="col-md-3 col-6">
                        <label class="form-control" for="food_name">Nome Piatto:</label> <!-- TODO : move to xml-->
                      </div>
                      <div class="col-md-3 col-6">
                        <input class="form-control" type="text" name="food_name" id="food_name" value="Nome Piatto*" required> <br>
                      </div>
                      <div class="col-md-3 col-6">
                        <select class="form-control" name="food_cat">
                                  <option value="1">Antipasto</option>
                                  <option value="2">Primo</option>
                                  <option value="3">Secondo'</option>
                                  <option value="4">Dolce'</option>
                                  <option value="5">Amaro</option>
                        </select>
                      </div>
                      <div class="col-md-3 col-6">
                        <select class="form-control" name="food_special">
                                  <option value="1">Nulla</option>
                                  <option value="2">Vegano</option>
                                  <option value="3">Senza Glutine'</option>
                                  <option value="4">Vegano e Senza Glutine'</option>
                        </select>
                      </div>

                      <div class="col-md-3 col-6">
                        <label class="form-control" for="des">Descrizione:</label> <!-- TODO : move to xml-->
                      </div>
                      <div class="col-md-3 col-6">
                        <textarea class="form-control" name="des" id="des" value="des">Descrizione e Ingredienti</textarea> <br>
                        <input type="hidden" name="desc" id="desc" value="" required>
                      </div>
                      <div class="col-md-3 col-6">
                        <label class="form-control" for="food_price">Prezzo:</label> <!-- TODO : move to xml-->
                      </div>
                      <div class="col-md-3 col-6">
                        <input class="form-control" type="text" name="food_price" id="food_price" value="Prezzo*" required> <br>
                      </div>
                      <input type="hidden" name="db_operation" id="db_operation" value="insert" required>
                      <input type="hidden" name="id_restaurant" id="id_restaurant" value="<?php echo $idresturant?>" required>
                      <div class="col-md-3 offset-md-3  col-5 offset-1 mb-3 center-block text-center">
                      <button id="button_confirm" class="form-control btn btn-green" type="submit">Conferma</button>
                    </div>
                    <div class="col-md-3 col-5 mb-3 center-block text-center">
                      <button id="button_back" class="form-control btn btn-red" type="submit">Annulla</button>
                    </div>
                    </div>
                  </form>
                </div>
            </div>
        </div>
    </div>

<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/footer.php'
?>
