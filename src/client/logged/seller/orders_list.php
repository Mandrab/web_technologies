<?php
    $page_name = 'Modify Menù';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/header.php'
?>

    </header>

    <script src="/src/client/logged/seller/orders_list.js"></script>

    <div class="container">
        <div class="row text-center">
            <div class="col-12">
                <h2 id="ordersTitle">Ordini</h2>
            </div>
        </div>
        <div class="row">
          <div id="elem_list" class="col-12">
            <?php
                foreach( $result as $row ) { ?>
                    <div id="<?php echo $row['id_order'] ?>" class="listblock" type="orders" class="row">
                        <div class="col-6">
                            <span id="namer"><?php echo $row['restaurant_name'] ?></span>
                        </div>
                        <div class="col-6">
                            <span id="nameu"><?php echo $row['user_name'] ?> <?php echo $row['surname'] ?></span>
                        </div>
                        <div class="col-6">
                            <span id="hour"><?php echo $row['hour'] ?></span>
                        </div>
                        <div class="col-6">
                            <span id="del_place"><?php echo $row['delivery_place'] ?></span>
                        </div>
                        <div class="col-6">
                            <span id="tot_cost"><?php echo $row['tot'] ?></span>
                        </div>
                        <input type="hidden" name="id_restaurant" id="id_restaurant" value="<?php echo $row['id_restaurant']?>">
                    </div> <?php
                }
            ?>
          </div>
        </div>
    </div>

<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/footer.php'
?>
