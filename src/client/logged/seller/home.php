<?php 
    $page_name = 'Seller Home';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/header.php'
?>

    </header>

    <?php
        require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/check_permission.php';
        checkAuth( array( 'seller' ) );
    ?>
    
    <script src="/src/client/logged/seller/home.js"></script>
    
    <div class="container">
        <div class="row mt-3 text-center">
            <div class="col-12">
                <h3 id="page_title" class="black text-center"></h3>
            </div>
        </div>
        <div class="row mt-3 mb-5">
            <div class="col-12 col-md-6 mt-2">
                <button id="restaurant_list" class="form-control btn btn-green">Lista ristoranti</button>
            </div>
            
            <div class="col-12 col-md-6 mt-2">
                <button id="order_list" class="form-control btn btn-green">Lista Ordini</button>
            </div>

            <div class="col-12 col-md-6 mt-2">
                <button id="notification_list" class="form-control btn btn-green">Notifiche</button>
            </div>

            <div class="col-12 col-md-6 mt-2">
                <button id="new" class="new form-control btn btn-green" type="button" name="button">Aggiungi Ristorante</button>
            </div>

            <div class="col-12 col-md-6 mt-2">
                <button id="logout" class="form-control btn btn-red">Logout</button>
            </div>

        </div>
    </div>
<?php 
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/footer.php'
?>