<?php
    $page_name = 'Modify Restaurant';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/header.php'
?>
    </header>
    <script src="/src/client/logged/seller/restaurant_mod.js"></script>

    <script src="/src/client/utility/img_loader/jquery.ui.widget.js"></script>
	  <script src="/src/client/utility/img_loader/jquery.iframe-transport.js"></script>
	  <script src="/src/client/utility/img_loader/jquery.fileupload.js"></script>
	  <script src="/src/client/utility/img_loader/main.js"></script>

    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2 id="restaurantTitle">Compila Ristorante</h2>
            </div>
        </div>
        <div class="row">
            <div id="elem_list" class="col-12 text-center">
                <?php // TODO: NON CAMBIATE L'id di nulla ?>
                <form id="image_form" action="/src/server/management/utils/upload.php" method="post" enctype="multipart/form-data" class="mt-5">
                  <div class="row">
                    <div class="col-md-3 col-6">
                      <label for="restaurant_name">Nome Locale:</label> <!-- TODO : move to xml-->
                    </div>
                    <div class="col-md-3 col-6">
                      <input type="text" name="restaurant_name" id="restaurant_name" class="form-control" value="<?php echo $result[0]['name']?> " required> <br>
                    </div>
                    <div class="col-md-3 col-6">
                      <label for="address">Indirizzo:</label> <!-- TODO : move to xml-->
                    </div>
                    <div class="col-md-3 col-6">
                      <input type="text" name="address" id="address" class="form-control" value="<?php echo $result[0]['address']?> " required> <br>
                    </div>
                    <div class="col-md-3 col-6">
                      <label for="tel">Telefono:</label> <!-- TODO : move to xml-->
                    </div>
                    <div class="col-md-3 col-6">
                      <input type="text" name="tel" id="tel" class="form-control" value="<?php echo $result[0]['tel']?>" required> <br>
                    </div>
                    <div class="col-md-6 col-12">
                      <input type="file" name="fileToUpload" class="form-control" id="fileToUpload">
                    </div>
                    <div class="col-md-3 col-6">
                      <label for="restaurant_type">Tipologia Locale:</label> <!-- TODO : move to xml-->
                    </div>
                    <div class="col-md-3 col-6">
                      <select id="types" name="types" class="form-control">
                          <?php
                          $c = 1;
                          foreach ($result2 as $r) {?>
                          <option value="<?php echo $c++ ?>" class="form-control"><?php echo $r['name'] ?></option>
                          <?php } ?>
                      </select>
                    </div>
                    <div class="col-md-3 col-6">
                      <label for="des">Descrizione:</label> <!-- TODO : move to xml-->
                    </div>
                    <div class="col-md-3 col-6">
                      <textarea name="des" id="des" value="des" class="form-control"><?php echo $result[0]['description']?>  </textarea> <br>
                    </div>
                    <div class="col-md-3 col-6">
                      <label for="iva">Partita IVA:</label> <!-- TODO : move to xml-->
                    </div>
                    <div class="col-md-3 col-6">
                      <input type="text" name="iva" id="iva" value="<?php echo $result[0]['p_iva']?>" class="form-control" required> <br>
                    </div>
                    <div class="col-md-3 col-12 mt-3 mb-3">
                      <select id="del_minutes" name="del_minutes" class="form-control">
                          <option value="00:05:00">5 minuti</option>
                          <option value="00:07:00">7 minuti</option>
                          <option value="00:10:00">10 minuti</option>
                          <option value="00:15:00">15 minuti</option>
                          <option value="00:20:00">20 minuti</option>
                          <option value="00:25:00">25 minuti</option>
                          <option value="00:30:00">30 minuti</option>
                          <option value="00:40:00">40 minuti</option>
                      </select>
                    </div>
                    <div class="col-md-3 col-12 mt-3 mb-3">
                      <button id="button_confirm" type="submit" class="form-control btn btn-green">Conferma</button>
                    </div>
                  </div>
                    <input type="hidden" name="id_restaurant" id="id_restaurant" value="<?php echo $idrestaurant ?>" required>
                    <input type="hidden" name="image" id="image" value="<?php echo $result[0]['photo_link']?>" required>
                    <input type="hidden" name="desc" id="desc" value="<?php echo $result[0]['description']?>" required>
                    <input type="hidden" name="active" id="active" value="1" required>
                    <input type="hidden" name="id_seller" id="id_seller" value="<?php echo $idseller ?>" required>
                </form>

                <form id="mod_data_openings" class="mt-5">
                  <div class="row">
                    <div class="col-4">
                      <select name="days" class="form-control">
                        <option value="1">Lunedì</option>
                        <option value="2">Martedì</option>
                        <option value="3">Mercoled'</option>
                        <option value="4">Gioved'</option>
                        <option value="5">Venerdì</option>
                        <option value="6">Sabato</option>
                        <option value="7">Domenica</option>
                      </select>
                    </div>
                    <div class="col-4">
                      <select name="open" class="form-control">
                        <option value="08:00:00">8:00</option>
                        <option value="08:30:00">8:30</option>
                        <option value="09:00:00">9:00</option>
                        <option value="09:30:00">9:30</option>
                        <option value="10:00:00">10:00</option>
                        <option value="10:30:00">10:30</option>
                        <option value="11:00:00">11:00</option>
                        <option value="11:30:00">11:30</option>
                        <option value="12:00:00">12:00</option>
                        <option value="12:30:00">12:30</option>
                        <option value="13:00:00">13:00</option>
                        <option value="13:30:00">13:30</option>
                        <option value="14:00:00">14:00</option>
                        <option value="14:30:00">14:30</option>
                        <option value="15:00:00">15:00</option>
                        <option value="15:30:00">15:30</option>
                        <option value="16:00:00">16:00</option>
                        <option value="16:30:00">16:30</option>
                      </select>
                    </div>
                    <div class="col-4">
                      <select name="close" class="form-control">
                        <option value="11:00:00">11:00</option>
                        <option value="11:30:00">11:30</option>
                        <option value="12:00:00">12:00</option>
                        <option value="12:30:00">12:30</option>
                        <option value="13:00:00">13:00</option>
                        <option value="13:30:00">13:30</option>
                        <option value="14:00:00">14:00</option>
                        <option value="14:30:00">14:30</option>
                        <option value="15:00:00">15:00</option>
                        <option value="15:30:00">15:30</option>
                        <option value="16:00:00">16:00</option>
                        <option value="16:30:00">16:30</option>
                        <option value="17:00:00">17:00</option>
                        <option value="17:30:00">17:30</option>
                        <option value="18:00:00">18:00</option>
                        <option value="18:30:00">18:30</option>
                        <option value="19:00:00">19:00</option>
                        <option value="19:30:00">19:30</option>
                        <option value="20:00:00">20:00</option>
                      </select>
                    </div>
                    <div class="col-12 mt-3 mb-3">
                      <table id="opening" style="width:100%">
                        <tr>
                          <th>Giorno</th>
                          <th>Apertura</th>
                          <th>Ciusura</th>
                        </tr>
                        <?php foreach ($result3 as $op) {
                          ?>
                            <tr>
                              <td><?php
                              switch ($op['week_day']) {
                                case '1':
                                  echo "Lunedì";
                                  break;
                                case '2':
                                  echo "Martedì";
                                  break;
                                case '3':
                                  echo "Mercoledì";
                                  break;
                                case '4':
                                  echo "giovedì";
                                  break;
                                case '5':
                                  echo "venerdì";
                                  break;
                                case '6':
                                  echo "Sabato";
                                  break;
                                case '7':
                                  echo "Domanica";
                                  break;

                                default:
                                  echo "error";
                                  break;
                              }?></td>
                              <td><?php echo $op['opening_hour'] ?></td>
                              <td><?php echo $op['closing_hour'] ?></td>
                            </tr>
                        <?php } ?>
                      </table>
                    </div>
                    <div class="col-6 mt-3 mb-3">
                      <button id="add" type="submit" class="form-control btn btn-green" value="">Aggiungi Giorno</button> <!-- TODO : aggiungere Azione-->
                    </div>
                    <div class="col-6 mt-3 mb-3">
                      <button id="button_clear" type="button" class="form-control btn btn-red" value="">Pulisci Tabella</button>
                    </div>
                  </div>
                  <input type="hidden" name="id_restaurant" id="id_restaurant" value="<?php echo $idrestaurant ?>" class="form-control" required>
                </form>
            </div>
        </div>
    </div>

<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/footer.php'
?>
