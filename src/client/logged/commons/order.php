<?php
    $page_name = 'View Order';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/header.php'
?>

    </header>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 id="ordersTitle">Ordini</h2>
            </div>
        </div>
        <div class="row">
          <div id="elem_list" class="col-12">
            <?php
                foreach( $result as $row ) { ?>
                    <div id="<?php echo $row['id_food'] ?>" val="<?php echo $row['id_food']?>" class="listblock row" type="order">
                        <div class="col-6 col-md-4">
                            <label id="namem">Nome Piatto:<?php echo $row['name'] ?></label>              /
                        </div>
                        <div class="col-6 col-md-4">
                            <label id="desc">Descrizione:<?php echo $row['description'] ?></label>        /
                        </div>
                        <div class="col-6 col-md-4">
                            <label id="price">Prezzo(X1):<?php echo $row['cost'] ?></label>               /
                        </div>
                        <div class="col-6 col-md-4">
                            <label id="qty">Quantità:<?php echo $row['quantity'] ?></label>               /
                        </div>
                    </div> <?php
                }
            ?>
          </div>
        </div>
    </div>

<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/footer.php'
?>
