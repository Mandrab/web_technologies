<?php
    $page_name = 'Restaurant';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/header.php'
?>

    </header>

    <?php
        require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/check_permission.php';
        checkAuth( array( 'admin', 'seller', 'user' ) );
    ?>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 id="page_title"></h2>
            </div>
        </div>
        <div id="restaurant" val="<?php echo $result['id']?>" class="row">
            <div class="col-12">
                <img id="restaurant_photo" src="<?php echo $result['photo'] ?>" alt="restaurant image" class="img-fluid">
            </div>
            <div class="col-6">
                <label id="restaurant_name"><?php echo $result['name'] ?></label>
            </div>
            <div class="col-md-4 col-10">
                <label id="restaurant_address"><?php echo $result['address'] ?></label>
            </div>
            <div class="col-4">
                <img id="restaurant_vote" src="/res/img/votes/<?php
                    echo ceiling( $result['vote'], 0.5 )
                    ?>_star.svg" alt="vote stars" class="img-fluid">
            </div>
            <div class="col-md-6 col-12">
                <label id="restaurant_description"><?php echo $result['description'] ?></label>
            </div>
        </div>
        <div class="text-center">
            <?php require $_SERVER['DOCUMENT_ROOT'] . '/src/client/logged/' . $_SESSION['user_type'] . '/snippet/restaurant.php' ?>
        </div>
    </div>

<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/footer.php'
?>

<?php

    function ceiling( $number, $significance = 1 ) {
        return ( is_numeric( $number ) && is_numeric( $significance ) )
            ? (ceil( $number/$significance) * $significance )
            : false;
    }

?>
