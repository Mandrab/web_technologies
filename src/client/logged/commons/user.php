<?php
    $page_name = 'User';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/header.php'
?>

    </header>
    
    <?php
        require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/check_permission.php';
        checkAuth( array( 'admin', 'seller', 'user' ) );
    ?>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 id="page_title"></h2>
            </div>
        </div>
        <div id="user" val="<?php echo $result['id']?>" class="row">
            <div class="col-6">
                <label for="name" id="l_name"></label>
            </div>
            <div class="col-6">
                <label id="name"><?php echo $result['name'] ?></label>
            </div>
            <div class="col-6">
                <label for="surname" id="l_surname"></label>
            </div>
            <div class="col-6">
                <label id="surname"><?php echo $result['surname'] ?></label>
            </div>
            <div class="col-6">
                <label for="tel" id="l_tel"></label>
            </div>
            <div class="col-6">
                <label id="tel"><?php echo $result['tel'] ?></label>
            </div>
            <div class="col-6">
                <label for="email" id="l_email"></label>
            </div>
            <div class="col-6">
                <label id="email"><?php echo $result['email'] ?></label>
            </div>
            <?php require $_SERVER['DOCUMENT_ROOT'] . '/src/client/logged/' . $_SESSION['user_type'] . '/snippet/user.php' ?>
        </div>
    </div>

<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/footer.php'
?>

<?php 

    function ceiling( $number, $significance = 1 ) {
        return ( is_numeric( $number ) && is_numeric( $significance ) ) 
            ? (ceil( $number/$significance) * $significance ) 
            : false;
    }

?>