<?php
    $page_name = 'List';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/header.php'
?>

    </header>

    <script src="/src/client/logged/commons/list.js"></script>

    <?php
        require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/check_permission.php';
        checkAuth( array( 'admin', 'seller', 'user' ) );
    ?>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 id="page_title"></h2>
            </div>
        </div>

        <div class="row">
            <div class="col-12 text-center">
                <?php require $_SERVER['DOCUMENT_ROOT'] . '/src/client/logged/' . $_SESSION['user_type'] . '/snippet/list_filter.php' ?>
                <label for="count">Risultati: </label>
                <label id="count"><?php echo count( $result ) ?></label>
            </div>
        </div>
        <div id="elem_list" class="row">
            <?php
                foreach( $result as $row ) {
                    if ( $_REQUEST['type'] == "restaurants" ) { ?>
                        <div id="<?php echo $row['id'] ?>" class="listblock col-md-4 col-12 list_block" type="restaurant" val="<?php echo $row['id'] ?>">
                            <div class="col-12">
                                <img src="<?php echo $row['photo_link'] ?>" alt="restaurant image" class="img-fluid">
                            </div>
                            <div class="col-12">
                                <label id="name"><?php echo $row['name'] ?></label>
                            </div>
                            <div class="col-12">
                                <label id="address"><?php echo $row['address'] ?></label>
                            </div>
                            <div class="col-12">
                                <img id="restaurant_vote" src="/res/img/votes/<?php
                                    if ( $row['active'] && $row['vote'] != '' )
                                        echo ceiling( $row['vote'], 0.5 );
                                    else
                                        echo '0';
                                ?>_star.svg" alt="vote stars" class="img-fluid">
                            </div>
                            <div class="col-12">
                                <label for="restaurant_num_vote">Numero voti:</label>
                                <label id="restaurant_num_vote"><?php echo $row['n_votes'] ?></label>
                            </div>
                            <div class="col-12">
                                <label id="restaurant_description"><?php echo $row['description'] ?></label>
                            </div>
                            <div class="col-12">
                                <?php if ( $_SESSION['user_type'] == 'admin' ) { ?>
                                    <label id="proprietary"><?php echo $row['owner_name'] ?></label>
                                    <label id="active"><?php echo $row['active'] ? "Active" : "Banned" ?></label>
                                <?php } else if ( $_SESSION['user_type'] == 'seller' ) { ?>
                                    <button id="modify_restaurant" val="<?php echo $row['id'] ?>" class="form-control btn btn-green mod"></button>
                                    <button id="modify_menu" val="<?php echo $row['id'] ?>" class="form-control btn btn-green menu"></button>
                                <?php } ?>
                            </div>
                        </div> <?php
                    }
                    require $_SERVER['DOCUMENT_ROOT'] . '/src/client/logged/' . $_SESSION['user_type'] . '/snippet/list_content.php';
                }
            ?>
        </div>
    </div>

<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/footer.php'
?>
