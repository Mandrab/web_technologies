<?php 
    $page_name = 'Notifications';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/header.php'
?>

    </header>

    <script src="/src/client/logged/commons/notifications.js"></script>

    <?php
        require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/check_permission.php';
        checkAuth( array( 'admin', 'seller', 'user' ) );
    ?>

    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2 id="page_title"></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 offset-md-5 col-12">
                <select id="filter">
                    <option id="select_all" value="all" <?php 
                        if ( !isset( $_REQUEST['filter'] ) || $_REQUEST['filter'] == 'all' ) 
                            echo 'selected=\"selected\"' 
                    ?>></option>
                    <option id="select_seen" value="seen" <?php 
                        if ( isset( $_REQUEST['filter'] ) && $_REQUEST['filter'] == 'seen' ) 
                            echo 'selected=\"selected\"' 
                    ?>></option>
                    <option id="select_unseen" value="unseen" <?php 
                        if ( isset( $_REQUEST['filter'] ) && $_REQUEST['filter'] == 'unseen' ) 
                            echo 'selected=\"selected\"' 
                    ?>></option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center">
                <label for="count">Risultati: </label>
                <label id="count"><?php echo count( $result ) ?></label>
            </div>
        </div>
        <div id="elem_list" class="row">
            <div class="col-4 text-center">
                <label for="data" id="l_data"></label>
            </div>
            <div class="col-4 text-center">
                <label for="notification_type" id="l_notification_type"></label> <!--TODO seller -->
            </div>
            <div class="col-4 text-center">
                <?php if ( isset( $row['id_order'] ) && $row['id_order'] != '' ) { ?>
                    <label for="order" id="l_order"></label>
                <?php } else { ?>
                    <label for="restaurant" id="l_restaurant"></label>
                <?php } ?>
            </div>
            <?php
                $result = array_reverse( $result );
                foreach( $result as $row ) { ?>
                    <div class="notice col-12 p-1 mb-1 list_block" <?php echo ( $row['seen'] ? 'seen' : 'unseen' ) ?>"
                        val="<?php echo $row['id']?>" type="<?php 
                            if ( isset( $row['id_order'] ) && $row['id_order'] != null ) 
                                echo "order"; 
                            else 
                                echo "restaurant";?>">
                        <div class="row">
                            <div class="col-4">
                                <label id="data"><?php echo $row['data'] ?></label>
                            </div>
                            <div class="col-4">
                                <label id="notification_type"><?php echo $row['type_descr'] ?></label>
                            </div>
                            <div class="col-4">
                            <?php if ( isset( $row['id_order'] ) && $row['id_order'] != '' ) { ?>
                                <label id="order">n° <?php echo $row['id_order'] ?></label>
                            <?php } else { ?>
                                <label id="restaurant"><?php echo $row['name'] ?></label>
                            <?php } ?>
                            </div>
                        </div>
                    </div> <?php
                }
            ?>
        </div>
    </div>

<?php 
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/footer.php'
?>