$( document ).ready( function() {

    $( "#filter" ).change( function() {
        window.location.href = window.location.href.split('?')[0] + "?filter=" + $( this ).val();
    } );

    $( ".notice" ).click( function() {
        window.location = "/src/server/management/commons/notice.php?id=" + $( this ).attr('val') + "&type=" + $( this ).attr('type');
    } );

} );