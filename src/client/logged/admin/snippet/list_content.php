<?php

    if ( $_REQUEST['type'] == "users" ) { ?>
    <div class="listblock row text-center" type="user" val="<?php echo $row['id'] ?>">
        <div class="col-4">
            <label id="name"><?php echo $row['name'] . $row['surname'] ?></label>
        </div>
        <div class="col-4">
            <label id="email"><?php echo $row['email'] ?></label>
        </div>
        <div class="col-4">
            <label id="active"><?php echo $row['active'] ? "Active" : "Banned" ?></label>
        </div>
    </div> <?php
    }