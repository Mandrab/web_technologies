$( document ).ready( function() {

    $( "#lock_button" ).click( function() {
        $.ajax( {
            type: "POST",
            url: "/src/server/management/admin/locker.php",
            data: { type: "user", id: $( "#user" ).attr( "val" ), operation: "deactive" }
        } ).done( function( ret ) {
            window.location.reload();
        } );
    } );

    $( "#unlock_button" ).click( function() {
        $.ajax( {
            type: "POST",
            url: "/src/server/management/admin/locker.php",
            data: { type: "user", id: $( "#user" ).attr( "val" ), operation: "active" }
        } ).done( function( ret ) {
            window.location.reload();
        } );
    } );

} );