<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/check_permission.php';
    checkAuth( array( 'admin' ) );
?>

<script src="/src/client/logged/<?php echo $_SESSION['user_type'] ?>/snippet/list_filter.js"></script>

<div class="row text-center">
    <div class="col-12">
        <select id="filter">
            <option id="select_all" value="all" <?php 
                if ( !isset( $_REQUEST['filter'] ) || $_REQUEST['filter'] == 'all' ) 
                    echo 'selected=\"selected\"' 
            ?>></option>

            <option id="select_lock" value="unactive" <?php 
                if ( isset( $_REQUEST['filter'] ) && $_REQUEST['filter'] == 'unactive' ) 
                    echo 'selected=\"selected\"' 
            ?>></option>

            <option id="select_unlock" value="active" <?php 
                if ( isset( $_REQUEST['filter'] ) && $_REQUEST['filter'] == 'active' ) 
                    echo 'selected=\"selected\"' 
            ?>></option>
        </select>
    </div>
</div>
