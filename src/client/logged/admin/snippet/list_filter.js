$( document ).ready( function() {

    $( '#filter' ).change( function() {
        var url = new URL( window.location.href );
        var type = url.searchParams.get( "type" );

        window.location.href = window.location.href.split('?')[0] 
            + "?type=" + type + "&filter=" + $( this ).val();
    } );

} );