<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/check_permission.php';
    checkAuth( array( 'admin' ) );
?>

<script src="/src/client/logged/admin/snippet/user.js"></script>

<label for="user_type" id="l_user_type"></label>
<label id="user_type"><?php echo $result['user_type'] ?></label>
<label for="orders" id="l_t_orders"></label>
<label id="orders"><?php echo $result['total_shop'] ?></label>
<label for="fav_rest" id="l_fav_rest"></label>
<label id="fav_rest"><?php echo $result['restaurant_name'] ?></label>
<label for="fav_rest_orders" id="l_fav_rest_orders"></label>
<label id="fav_rest_orders"><?php echo $result['shop_in_restaurant'] ?></label>

<button id="lock_button" type="button" <?php 
    if ( !$result['active'] || $_SESSION['email'] == $result['email'] ) 
        echo "hidden"; 
?>></button>

<button id="unlock_button" type="button" <?php 
    if ( $result['active'] || $_SESSION['email'] == $result['email'] ) 
        echo "hidden"; 
?>></button>