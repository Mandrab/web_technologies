$( document ).ready( function() {

    $( "#lock_button" ).click( function() {
        $.ajax( {
            type: "POST",
            url: "/src/server/management/admin/locker.php",
            data: { type: "restaurant", id: $( "#restaurant" ).attr( "val" ), operation: "deactive" }
        } ).done( function( ret ) {
            window.location.reload();
        } );
    } );

    $( "#unlock_button" ).click( function() {
        $.ajax( {
            type: "POST",
            url: "/src/server/management/admin/locker.php",
            data: { type: "restaurant", id: $( "#restaurant" ).attr( "val" ), operation: "active" }
        } ).done( function( ret ) {
            window.location.reload();
        } );
    } );

    $( "#accept_button" ).click( function() {
        $.ajax( {
            type: "POST",
            url: "/src/server/management/admin/locker.php",
            data: { type: "restaurant", id: $( "#restaurant" ).attr( "val" ), operation: "accept" }
        } ).done( function( ret ) {
            if ( ret == 'ok' ) {
                if ( window.location.href.split('/').reverse()[0].split('?')[0] == "notice.php" )
                    window.location.href = '/src/server/management/commons/notifications.php';
                else if ( window.location.href.split('/').reverse()[0].split('?')[0] == "restaurant.php" )
                    window.location.href = '/src/server/management/commons/list.php';
                else
                    window.location.href = '/src/server/management/admin/home.php';
            }
        } );
    } );

    $( "#reject_button" ).click( function() {
        $.ajax( {
            type: "POST",
            url: "/src/server/management/admin/locker.php",
            data: { type: "restaurant", id: $( "#restaurant" ).attr( "val" ), operation: "reject" }
        } ).done( function( ret ) {
            if ( ret == 'ok' ) {
                if ( window.location.href.split('/').reverse()[0].split('?')[0] == "notice.php" )
                    window.location.href = '/src/server/management/commons/notifications.php';
                else if ( window.location.href.split('/').reverse()[0].split('?')[0] == "restaurant.php" )
                    window.location.href = '/src/server/management/commons/list.php';
                else
                    window.location.href = '/src/server/management/admin/home.php';
            }
        } );
    } );

} );