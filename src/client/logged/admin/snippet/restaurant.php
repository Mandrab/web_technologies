<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/check_permission.php';
    checkAuth( array( 'admin' ) );
?>

<script src="/src/client/logged/admin/snippet/restaurant.js"></script>

<button class="btn btn-red" id="lock_button" type="submit" <?php 
    if ( !$result['active'] || $mode == "accept" ) 
        echo "hidden"; 
?>></button>

<button class="btn btn-green" id="unlock_button" type="submit" <?php 
    if ( $result['active'] || $mode == "accept" ) 
        echo "hidden"; 
?>></button>

<button class="btn btn-green" id="accept_button" type="submit" <?php 
    if ( !isset( $mode ) || $mode != "accept" ) 
        echo "hidden"; 
?>></button>

<button class="btn btn-red" id="reject_button" type="submit" <?php 
    if ( !isset( $mode ) || $mode != "accept" ) 
        echo "hidden"; 
?>></button>