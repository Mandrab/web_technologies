$( document ).ready( function() {

    $( "#restaurant_list" ).click( function() {
        window.location.href = "/src/server/management/commons/list.php?type=restaurants";
    } );

    $( "#user_list" ).click( function() {
        window.location.href = "/src/server/management/commons/list.php?type=users";
    } );

    $( "#notification_list" ).click( function() {
        window.location.href = "/src/server/management/commons/notifications.php";
    } );

    $( "#logout" ).click( function() {
        window.location.href = "/src/server/access/unlogger.php";
    } );

} );