<?php 
    $page_name = 'Admin_Home';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/header.php'
?>

    </header>

    <?php
        require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/check_permission.php';
        checkAuth( array( 'admin' ) );
    ?>
    
    <script src="/src/client/logged/admin/home.js"></script>
    
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2 id="page_title"></h2>
            </div>
        </div>
        <div class="row mb-5 mt-5">
            <div class="col-md-6 col-12 mt-2">
                <button id="restaurant_list" class="form-control btn btn-green"></button>
            </div>
            <div class="col-md-6 col-12 mt-2">
                <button id="user_list"  class="form-control btn btn-green"></button>
            </div>
            <div class="col-md-6 col-12 mt-2">
                <button id="notification_list"  class="form-control btn btn-green"></button>
            </div>
            <div class="col-md-6 col-12 mt-2">
                <button id="logout"  class="form-control btn btn-red"></button>
            </div>
        </div>
    </div>

<?php 
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/footer.php'
?>