$( document ).ready( function() {

    $( "#log_data_input" ).submit( function() {

        $.ajax({
            method: "POST",
            url: "/src/server/access/logger.php",
            data: $( this ).serialize()
        }).done( function( res ) {
            console.log(res);
            if ( res != "err: wrong input" )
                window.location.href = res;
            else
                $( "#wrong_data" ).attr( "hidden", false );
        });

        return false;
    });
    
    $( "#t_find_out_more_button" ).click( function() {
        window.location.href = '/signin.php?type=seller';
    });

} );