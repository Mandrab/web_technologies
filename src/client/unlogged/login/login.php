<?php 
    $page_name = "Login";
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/header.php'
?>
	</header>

    <script src="/src/client/unlogged/login/login.js"></script>

	<div class="container-fluid log-in">
		<div class="row">
			<div class="col-12 text-center">
				<h2 id="page_title" class="white"></h2>
			</div>
		</div>
		<div class="row">
			<div class="col-4 offset-4 text-center white-back">
				<label id="register_question"></label>
				<a href="/signin.php" id="register_link_text">signin</a>
			</div>
		</div>
		<div class="row"> 
			<form id="log_data_input" class="col-12">
				<div class="row">
					<div class="col-md-4 offset-md-2 col-12 " style="color: white">
						<label for="email" id="t_email"></label> <!-- TODO : move to xml-->
						<input type="text" class="form-control" name="email" id="email" value="e-mail" autocomplete="email"> <br>
					</div>
					<div class="col-md-4 col-12" style="color: white">
						<label for="password" id="t_password"></label> <!-- TODO : move to xml-->
						<input type="password" class="form-control" name="password" id="password" value="password" placeholder="Enter Password" autocomplete="current-password" />
					</div>
				</div>
				<div class="row">
					<div class="col-md-2 offset-md-5 col-4 offset-4 text-center mt-4" style="color: white">
						<button id="button_login" type="submit" class="form-control"></button>
						<label class="err" id="wrong_data" for="log_data_input" hidden>Wrong Input!</label>
					</div>
				</div>
                <div class="row text-center">
                    <div class="col-12 text-center">
                    <a href="/signin.php?type=seller">Registrati come venditore</a>
                    </div>
                </div>
            </form>
        </div>
	</div>

<?php 
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/footer.php'
?>