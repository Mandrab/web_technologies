$( document ).ready( function() {

    //Submit event
    //Preferito a click per svariati motivi, tra cui la verifica di html5 dei campi required e l'utilizzo del metodo serialize di jquery
    $( "#form" ).submit( function() {
        //Controllo che le password siano uguali
        if ( $( "#password" ).val() == $( "#password_confirm" ).val() ) {

            $( '#wrong_data' ).attr( "hidden", true );

            //Chiamata ajax
            $.ajax( {
                method: "POST",
                url: "/src/server/access/register.php", 
                data: $( this ).serialize()
            } ).done( function ( ret ) {
                console.log(ret);
                //Se la mail è già presente mostro messaggio di errore
                if( ret == 'ok' )            
                    window.location.href = '/src/server/management/' + $( '#user_type' ).val() + '/home.php';
                else
                    $( ".error" ).show();
            } );
        } else {
            $( '#wrong_data' ).attr( "hidden", false );
        }
        return false;
    } );

    $( "#t_find_out_more_button" ).click( function() {
        window.location.href = "/signin.php?type=seller";
    } );

} );
