<?php 
    $page_name = "Signin";
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/header.php'
?>
    </header>

    <script src="/src/client/unlogged/signin/signin.js"></script>

    <div class="container-fluid log-in">
        <?php if ( isset( $_REQUEST['type'] ) && $_REQUEST['type'] == 'seller' ) { ?>
            <div class="bg-white" style="padding: 10px 10px; border-radius: 10px"  id="seller_login_info_box">
                <h2 id="info_title"></h2>
                <p id="info_text"></p>
            </div>
        <?php } ?>
        <div >
            <div class="row">
                <div class="col-12 text-center">
                    <h2 class="white" id="page_title"></h2>
                </div>
            </div>
            <form id="form">
                <div class="row">
                    <div class="col-md-4 offset-md-2 col-12">
                        <label class="white" id="name_l" for="name"></label>
                        <input type="text" class="form-control" name="name" id="name" value="name" required> <br>
                    </div>
                    <div class="col-md-4 col-12">
                        <label class="white" id="surname_l" for="surname"></label>
                        <input type="text" class="form-control" name="surname" id="surname" value="surname" required> <br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 offset-md-2 col-12">
                        <label class="white" id="tel_l" for="tel"></label>
                        <input type="text" class="form-control" name="tel" id="tel" value="xxx.xxxxxxx" autocomplete="tel" required> <br>
                    </div>
                    <div class="col-md-4 col-12">
                        <label class="white" id="email_l" for="email"></label>
                        <input type="text" class="form-control" name="email" id="email" value="e-mail" autocomplete="Email" required> <br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 offset-md-2 col-12">
                        <label class="white" id="password_l" for="password"></label>
                        <input type="password" class="form-control" name="password" id="password" value="password" placeholder="Enter Password" autocomplete="current-password" required><br>
                    </div>
                    <div class="col-md-4 col-12">
                        <label class="white" id="password_confirm_l" for="password_confirm"></label>
                        <input type="password" class="form-control" name="password_confirm" id="password_confirm" value="password" autocomplete="new-password" required> <br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4 offset-4 text-center">
                    <?php if ( isset( $_REQUEST['type'] ) && $_REQUEST['type'] == 'seller' ) { ?>
                        <input type="text" name="user_type" id="user_type" value="seller" required hidden>
                    <?php } else { ?>
                        <input type="text" name="user_type" id="user_type" value="user" required hidden>
                    <?php } ?>
                    <button id="confirm" type="submit" class="btn btn-green"></button>
                    </div>
                    <label class="err" id="wrong_data" for="log_data_input" hidden></label>
                    <span class="error" id="email_present_error" style="display:none"></span>
                </div>
                
            </form>
            
        </div>
    </div>
<?php 
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/footer.php'
?>