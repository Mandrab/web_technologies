<!DOCTYPE html>
<html lang=it>
    <head>
        <title><?php echo $page_name ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Font-Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <!-- CSS Bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        <!-- Nostro CSS -->
        <link rel="stylesheet" href="/src/client/common.css">
        <!-- JS Bootstrap/Jquery -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>    
        <script src="/src/client/utility/traduction/html_loader.js"></script>
        <script src="/src/client/header.js"></script>
    </head>

    <body>
        <header>
            <div id="home_redirect" class="container-fluid">
                <div class="row">
                    <div class="col-2 offset-1">
                        <img id="site_image" src="/res/img/unibo_logo.svg" alt="logo" class="img-fluid">
                    </div>
                    <div class="col-6 align-self-center">
                        <h1 id="site_name" class="text-center"></h1>
                    </div>
                    <div class="col-2">
                    <?php if ( $page_name != 'Login' && $page_name != 'Signin' ) { ?>
                        <script src="/src/client/utility/notifications/checker.js"></script>
                        <img id="notifications_ico" src="/res/img/pages_icon/<?php 
                            if( $_SESSION['unseen_notifications'] )
                                echo 'notifications';
                            else
                                echo 'no_notifications';
                            ?>.svg" alt="notification icon" class="img-fluid">
                    <?php } else { ?>
                        <img id="site_image" src="/res/img/unibo_logo.svg" alt="logo" class="img-fluid">
                    <?php } ?>
                    </div>
                </div>
            </div>
            
