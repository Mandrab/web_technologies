        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-3 offset-md-2 col-5 offset-2">
                        <p id="t_privacy_information"></p>
                    </div>
                    <div class="col-md-2 col-4">
                        <i class="fas fa-plus-circle"></i>
                    </div>
                    <div class="col-md-2 offset-md-0 col-5 offset-2">
                        <p id="t_food"></p>
                    </div>
                    <div class="col-md-2 col-4">
                        <i class="fas fa-plus-circle"></i>
                    </div>
                </div>
            </div>
		</footer>
    </body>
</html>
