$( document ).ready( function () {

    $.ajax( {
        method: "GET",
        url: window.location.origin + "/res/lang/it.xml"
    } ).done( ret => translate( ret ) );

} );

function translate( xmlDoc ) {

    var xml = $( xmlDoc );

    // if in the XML exist a subpart for the page, search in that
    var page = $( "title" ).text().toLowerCase() + "_page";

    $( "[class]" ).each( function ( i, obj ) {
        // search the tag in the XML
        var res = xml.find( page ).find( obj.className )
        if ( res.text() == "" )
            res = $( xml.find( "general" ).find( obj.className ) );
        if ( res.text() != "" )
            updateElem( obj, res );
    } );

    $( "[id]" ).each( function ( i, obj ) {
        // search the tag in the XML
        var res = xml.find( page ).find( obj.id )
        if ( res.text() == "" )
            res = $( xml.find( "general" ).find( obj.id ) );
        if ( res.text() != "" )
            updateElem( obj, res );
    } );

}

function updateElem( obj, res ) {

    switch ( obj.nodeName ) {
        case "INPUT":
            obj.value = res.text();
            break;
        case "IMG":
            obj.src = res.find( "src" ).text();
            obj.alt = res.find( "alt" ).text();
            break;
        default:
            $( obj ).text( res.text() );
    }
}