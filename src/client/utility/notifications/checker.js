// this could be done with better method but it still work

$( document ).ready( function() {

    var saved_ret = 0;
    $.get( '/src/server/management/utils/notifier.php', function( ret ){
        saved_ret = ret;
    } );

    var refreshId = setInterval( function() {  
        $.ajax( {
            type: 'GET',
            url: '/src/server/management/utils/notifier.php',
            success: function( ret ) {
                if ( ret != saved_ret 
                    && ( window.location.href.split('/').pop().split('?')[0] == 'notifications.php'
                    || window.location.href.split('/').pop().split('?')[0] == 'list.php' ) )
                    window.location.reload();
                if ( ret == '0' && $( "#notifications_ico" ).attr( 'src' ) == '/res/img/pages_icon/notifications.svg' )
                    $( "#notifications_ico" ).attr( 'src', '/res/img/pages_icon/no_notifications.svg' );
                if ( ret != '0' && $( "#notifications_ico" ).attr( 'src' ) == '/res/img/pages_icon/no_notifications.svg' )
                    $( "#notifications_ico" ).attr( 'src', '/res/img/pages_icon/notifications.svg' );
            }
        } );
    }, 1000 );

} );
