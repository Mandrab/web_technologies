<?php

    session_start();

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/check_permission.php';
    checkAuth( array( 'admin', 'seller', 'user' ) );

    session_destroy();

    header( 'Location: /index.php' );

    exit;