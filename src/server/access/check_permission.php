<?php

    session_start();

    function checkAuth( $authorized_users ) {

        if ( !isset( $_SESSION['user_type'] ) ) {

            header( 'Location: /index.php' );
            exit;

        }

        if ( !in_array( $_SESSION['user_type'], $authorized_users ) ) {              // if logged user hasn't auth

            header( 'Location: /src/server/management/' . $_SESSION['user_type'] . '/home.php' );
            exit;

        }

    }