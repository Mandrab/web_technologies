<?php
//Dati per connetersi al db online
require_once $_SERVER['DOCUMENT_ROOT'] . "/src/server/access/db_params.php";
//Portarsi dietro la funzione di check
require_once $_SERVER['DOCUMENT_ROOT'] . "/src/server/access/check_permission.php";

try {
    $db = new PDO('mysql:host=' . $db_servername . ';dbname=' . $db_name . ';charset=utf8', $db_username, $db_password, array(
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
    ));
} catch (PDOException $e) {
    $desc = 'Errore di connessione al server. Riprovare.';
    echo $desc;
    die();
}