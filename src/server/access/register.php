<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/db_connection.php';

    $elem = array('name', 'surname', 'tel', 'email', 'password', 'user_type');
    $err = array();
    $exit = false;

    // Check datas
    foreach ( $elem as $e )
        if ( !isset( $_REQUEST[$e] ) || $_REQUEST[$e] == "" ) {
            echo "Err: " . $e . " is not set\n";
            $exit = true;
        }
    if ( $exit ) 
        die("err: wrong input");

    $name = $_REQUEST['name'];
    $surname = $_REQUEST['surname'];
    $tel = $_REQUEST['tel'];
    $email = $_REQUEST['email'];
    $password = $_REQUEST['password'];
    $user_type = $_REQUEST['user_type'];

    $query = "INSERT INTO users(name, surname, tel, email, password, user_type) 
        VALUES (:name, :surname, :tel, :email, :password, :user_type)";
    $db->beginTransaction();
    $stmt = $db->prepare($query);
    $stmt->bindParam(":name", $name);
    $stmt->bindParam(":surname", $surname);
    $stmt->bindParam(":tel", $tel);
    $stmt->bindParam(":email", $email);
    $stmt->bindParam(":password", $password);
    $stmt->bindParam(":user_type", $user_type);
    $stmt->execute();
    $db->commit();

    $_SESSION["user_type"] = $user_type;
    $_SESSION["email"] = $email;

    echo "ok";