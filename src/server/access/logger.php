<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/db_connection.php';

    // Get datas
    if ( !isset( $_REQUEST['email'] ) || !isset( $_REQUEST['password'] ) )
        die( "err: wrong input" );
    $email = $_REQUEST['email'];
    $password = $_REQUEST['password'];

    $query = "SELECT * FROM users
        WHERE email = :email";
    $db->beginTransaction();
    $stmt = $db->prepare($query);
    $stmt->bindParam(":email", $email);
    $stmt->execute();
    $db->commit();

    $id ="";
    $role ="";
    $pwdDB = "";
    $active = 0;

    if ( $res = $stmt->fetch( PDO::FETCH_ASSOC ) ) {
        $id = $res["id"];
        $role = $res["user_type"];
        $pwdDB = $res["password"];
        $active = $res["active"];
    }

    if( $password == $pwdDB && $active ){

        $query = 'SELECT COUNT(id) AS n FROM notifications WHERE id_receiver=:id AND seen=0';
        $db->beginTransaction();
        $stmt = $db->prepare($query);
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        $db->commit();

        $_SESSION["user_type"] = $role;
        $_SESSION["email"] = $email;
        $_SESSION["id"] = $id;
        $_SESSION["unseen_notifications"] = $stmt->fetch()['n'];
        
        echo "/src/server/management/" . $role . "/home.php";
    } else
        echo "err: wrong input";
