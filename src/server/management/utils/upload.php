<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/check_permission.php';
    checkAuth( array( 'seller' ) );

    if ( isset( $_FILES["fileToUpload"]["name"] ) && $_FILES["fileToUpload"]["name"] != '' ) {

        $target_dir = $_SERVER['DOCUMENT_ROOT'] . "/res/img/restaurants/";
        $exploded = explode( '.', basename( $_FILES["fileToUpload"]["name"] ) );
        $file_name = 'rest_' . $_REQUEST['id_restaurant'] . '.' . array_pop( $exploded );

        $target_file = $target_dir . $file_name;
        $imageFileType = strtolower( pathinfo( $target_file, PATHINFO_EXTENSION ) );    

        if( isset( $_POST["submit"] ) ) {
            
            $check = getimagesize( $_FILES["fileToUpload"]["tmp_name"] );
            
            if( $check === false )
                die( 'err: not an image' );                                                 //File is not an image.

        }

        // Delete file if already exist
        if ( file_exists( $target_file ) )
            unlink($target_file);

        // Check file size
        if ( $_FILES["fileToUpload"]["size"] > 5000000 )
            die( "Sorry, your file is too large." );

        // Allow certain file formats
        if( $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" && $imageFileType != "svg" )
            die( "Sorry, only JPG, JPEG, PNG, GIF & SVG files are allowed." );

        // if everything is ok, try to upload file
        move_uploaded_file( $_FILES["fileToUpload"]["tmp_name"], $target_file );

    }

    if ( isset( $target_file ) && file_exists( $target_file ) )
        $_REQUEST['image'] = '/res/img/restaurants/' . $file_name;
    else
        $_REQUEST['image'] = '/res/img/restaurants/no_image.svg';

    $_REQUEST['type'] = 'restaurant';

    require $_SERVER['DOCUMENT_ROOT'] . '/src/server/management/seller/restaurant_inserter.php'

?>
