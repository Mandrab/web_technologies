<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/db_connection.php';
    
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/check_permission.php';
    checkAuth( array( 'admin', 'seller', 'user' ) );

    if ( isset( $type ) ) {
        $query = 'INSERT INTO notifications 
            VALUES (0, :time, :type, 0, :id_order, :id_restaurant, :notification_type, :id_receiver)';
        $db->beginTransaction();
        $stmt = $db->prepare($query);

        $time = date("H:i:s",time());

        if( $type == 'new restaurant') {
            $id_notification_type = '1';
            $id_receiver = '1';
        }
        if( $type == 'restaurant rejected' )
            $id_notification_type = '2';
        if( $type == 'restaurant accepted' )
            $id_notification_type = '3';
        if( $type ==  'restaurant locked' )
            $id_notification_type = '4';
        if( $type ==  'restaurant unlocked' )
            $id_notification_type = '5';

        if( $type == 'new order' )
            $id_notification_type = '6';
        if( $type == 'new order status' )
            $id_notification_type = '7';


        $stmt->bindParam( ':type', $type );
        $stmt->bindParam( ':time', $time );
        $stmt->bindParam( ':id_order', $id_order );
        $stmt->bindParam( ':id_restaurant', $id_restaurant );
        $stmt->bindParam( ':notification_type', $id_notification_type );
        $stmt->bindParam( ':id_receiver', $id_receiver );     
        $stmt->execute();
        $db->commit();

        $_SESSION['unseen_notifications']++;

    } else {
        echo $_SESSION['unseen_notifications'];
    }