<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/check_permission.php';
    checkAuth( array( 'admin', 'seller', 'user' ) );

    require $_SERVER['DOCUMENT_ROOT'] . '/src/server/management/user/order.php';