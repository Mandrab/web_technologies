<?php

    // TODO : check auth

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/db_connection.php';

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/check_permission.php';
    checkAuth( array( 'admin', 'seller', 'user' ) );

    $id = $_REQUEST['id'];

    $query = "SELECT * FROM notifications WHERE id=:id";

    $db->beginTransaction();
    $stmt = $db->prepare($query);
    $stmt->bindParam(":id", $id);
    $stmt->execute();
    $db->commit();

    if ($stmt->fetch()['seen'] == 0 )
        $_SESSION['unseen_notifications']--;

    $query = "UPDATE notifications SET seen=TRUE WHERE id=:id";

    $db->beginTransaction();
    $stmt = $db->prepare($query);
    $stmt->bindParam(":id", $id);
    $stmt->execute();
    $db->commit();

    $query = "SELECT * FROM notifications WHERE id=:id";

    $db->beginTransaction();
    $stmt = $db->prepare($query);
    $stmt->bindParam(":id", $id);
    $stmt->execute();
    $db->commit();

    $mode = "accept";
    $result = $stmt->fetch();

    if ( $_REQUEST['type'] == 'order' ) {
        $_REQUEST['idorder'] = $result['id_order'];
        require $_SERVER['DOCUMENT_ROOT'] . '/src/server/management/' . $_SESSION['user_type'] . '/order.php';
    } else if ( $_REQUEST['type'] == 'restaurant' )
        require $_SERVER['DOCUMENT_ROOT'] . '/src/server/management/commons/restaurant.php';
    else
        die( 'err' );