<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/db_connection.php';

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/check_permission.php';
    checkAuth( array( 'admin', 'seller', 'user' ) );

    require $_SERVER['DOCUMENT_ROOT'] . '/src/server/management/' . $_SESSION['user_type'] . '/list.php';

    $stmt->execute();
    $db->commit();

    $result = $stmt -> fetchAll();

    require $_SERVER['DOCUMENT_ROOT'] . '/src/client/logged/commons/list.php';



    function ceiling( $number, $significance = 1 ) {
        return ( is_numeric( $number ) && is_numeric( $significance ) )
            ? (ceil( $number/$significance) * $significance )
            : false;
    }
