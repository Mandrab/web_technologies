<?php

    // TODO : check auth

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/db_connection.php';

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/check_permission.php';
    checkAuth( array( 'admin', 'seller', 'user' ) );

    $id = ( isset( $result['id_restaurant'] )
        ? $result['id_restaurant']
        : (isset( $_REQUEST['id'] )
            ? $_REQUEST['id']
            : die( 'err: no restaurant id' ) ) );

    $query = "SELECT *, photo_link AS photo FROM restaurants LEFT JOIN (
                SELECT AVG(val) AS vote, id_restaurant, COUNT(id) AS n_votes
                FROM votes
                GROUP BY id_restaurant ) AS votes
            ON restaurants.id = votes.id_restaurant
            WHERE restaurants.id=:id";

    $db->beginTransaction();
    $stmt = $db->prepare( $query );
    $stmt->bindParam( ":id", $id );
    $stmt->execute();
    $db->commit();

    $result = $stmt->fetch();

    require $_SERVER['DOCUMENT_ROOT'] . '/src/client/logged/commons/restaurant.php';
