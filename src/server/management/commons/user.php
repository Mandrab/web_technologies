<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/check_permission.php';
    checkAuth( array( 'admin', 'seller', 'user' ) );

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/db_connection.php';

    $id = isset( $_REQUEST['id'] )
        ? $_REQUEST['id']
        : ( isset( $result['id_user'] )
            ? $result['id_restaurant']
            : die( 'err: no user id' ) );

    $query = 'SELECT u.id, u.name, u.surname, u.tel, u.email, u.active, u.user_type, 
            id_restaurant, r.name AS restaurant_name, shop_in_restaurant, SUM(shop_in_restaurant) AS total_shop
            FROM users AS u JOIN (SELECT id_client, id_restaurant, 
                COUNT(DISTINCT(id_order)) AS shop_in_restaurant
                FROM order_foods AS of
                JOIN orders ON id_order=orders.id
                JOIN foods ON id_food=foods.id
                GROUP BY id_restaurant) AS client_restaurant
            ON u.id=id_client JOIN restaurants AS r 
            ON r.id=id_restaurant
            WHERE u.id=:id
            ORDER BY shop_in_restaurant DESC
            LIMIT 1';

    $db->beginTransaction();
    $stmt = $db->prepare( $query );
    $stmt->bindParam( 'id', $id, PDO::PARAM_INT );
    $stmt->execute();
    $db->commit();

    $result = $stmt->fetch();

    require $_SERVER['DOCUMENT_ROOT'] . '/src/client/logged/commons/user.php';