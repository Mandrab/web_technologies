<?php

    // TODO : check auth

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/db_connection.php';

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/check_permission.php';
    checkAuth( array( 'admin', 'seller', 'user' ) );

    $query = "SELECT r.*, n.data, n.description, n.seen, n.id_restaurant, r.name, 
            nt.description AS type_descr, n.id FROM notifications AS n
            JOIN notification_types AS nt 
            ON id_notification_type=nt.id
            JOIN restaurants AS r ON id_restaurant = r.id
            WHERE id_receiver=(SELECT id FROM users 
                WHERE email=:email)
            AND n.id_restaurant IS NOT NULL";

    if ( isset( $_REQUEST[ 'filter' ] ) && $_REQUEST[ 'filter' ] != 'all' )
        $query = $query . ( $_REQUEST[ 'filter' ] == "seen" 
            ? " AND n.seen" : " AND NOT n.seen" );

    $db->beginTransaction();
    $stmt = $db->prepare( $query );
    $stmt->bindParam( ":email", $_SESSION['email'] );
    $stmt->execute();
    $db->commit();
    
    $result = $stmt->fetchAll();

    $query = "SELECT n.id, n.data, n.description, n.seen, n.id_order, 
            nt.description AS type_descr, o.hour, o.status, 
            o.delivery_place FROM notifications AS n
            JOIN notification_types AS nt 
            ON id_notification_type=nt.id
            JOIN orders AS o ON id_order=o.id
            WHERE id_receiver=(SELECT id FROM users 
                WHERE email=:email)";

    if ( isset( $_REQUEST[ 'filter' ] ) && $_REQUEST[ 'filter' ] != 'all' )
        $query = $query . ( $_REQUEST[ 'filter' ] == "seen" 
            ? " AND n.seen" : " AND NOT n.seen" );

    $db->beginTransaction();
    $stmt = $db->prepare( $query );
    $stmt->bindParam( ":email", $_SESSION['email'] );
    $stmt->execute();
    $db->commit();
    
    $result = array_merge( $result, $stmt->fetchAll() );

    require $_SERVER['DOCUMENT_ROOT'] . '/src/client/logged/commons/notifications.php';

?>