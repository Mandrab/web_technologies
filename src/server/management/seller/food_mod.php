<?php

    // TODO : check auth

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/db_connection.php';

    $idseller = $_SESSION["id"];
    $idfood = $_REQUEST['idfood'];


    $query = "SELECT `foods`.*, `food_types`.description AS tdes , `food_types`.`name` AS tipo  FROM foods JOIN food_types ON `foods`.id_food_type = `food_types`.id
              WHERE `foods`.`id` = :idfood AND `active` = 1 ORDER BY tipo";

    $db->beginTransaction();
    $stmt = $db->prepare($query);
    $stmt->bindParam(":idfood", $idfood);
    $stmt->execute();
    $db->commit();

    $result = $stmt -> fetchAll();

    // var_dump($result[0]['description']);
    // var_dump($result);
    // die();

    require $_SERVER['DOCUMENT_ROOT'] . '/src/client/logged/seller/food_mod.php';
