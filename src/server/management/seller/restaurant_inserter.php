<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/db_connection.php';


    if(isset($_SESSION['new_restaurant'])){
      unset($_SESSION['new_restaurant']);
    }

    $idrestaurant = $_REQUEST['id_restaurant'];
    $name = $_REQUEST['restaurant_name'];
    $address = $_REQUEST['address'];
    $tel = $_REQUEST['tel'];
    $p_iva = $_REQUEST['iva'];
    $photo_link = $_REQUEST['image'];
    $description = $_REQUEST['desc'];
    $del = $_REQUEST['del_minutes'];
    $id_owner = $_REQUEST['id_seller'];
    $type = $_REQUEST['types'];

    $query = "UPDATE `restaurants`
    SET `name`= :name ,`address`= :address,`tel`=:tel,`p_iva`=:p_iva,`photo_link`=:photo_link,`description`= :des,`del_predicted_time`= :del_minutes,`id_owner`=:id_owner,`id_restaurant_type`=:type
    WHERE `id`= :id_restaurant";

    $db->beginTransaction();
    $stmt = $db->prepare($query);
    $stmt->bindParam(":name", $name);
    $stmt->bindParam(":address", $address);
    $stmt->bindParam(":tel", $tel);
    $stmt->bindParam(":p_iva", $p_iva);
    $stmt->bindParam(":photo_link", $photo_link);
    $stmt->bindParam(":des", $description);
    $stmt->bindParam(":del_minutes", $del);
    $stmt->bindParam(":id_owner", $id_owner);
    $stmt->bindParam(":type", $type);
    $stmt->bindParam(":id_restaurant", $idrestaurant);
    $stmt->execute();
    $db->commit();

    require $_SERVER['DOCUMENT_ROOT'].'/src/server/management/seller/home.php';
