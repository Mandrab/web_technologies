<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/db_connection.php';

    $idseller = $_SESSION["id"];
    $hournow = $_REQUEST['hour'];

    // SELECT of.id AS id_food_order,`orders`.id AS id_order,
    //           `orders`.hour,`orders`.status,`orders`.delivery_place,`orders`.id_client,
    //           `orders`.id_delivery_man,`foods`.id as id_food,`foods`.name as food_name,
    //           `foods`.description,`foods`.gluten_free,`foods`.vegan,`foods`.cost,`foods`.active,
    //           `foods`.id_food_type,`foods`.id_restaurant,`restaurants`.name AS restaurant_name,
    //           `restaurants`.active AS restaurant_active,`restaurants`.id_owner
    //           FROM order_foods AS of
    //           JOIN orders ON id_order=`orders`.id
    //           JOIN foods ON id_food=`foods`.id
    //           JOIN restaurants ON id_restaurant=`restaurants`.id
    //           WHERE `id_owner` = 3 AND `foods`.`active`= 1 AND `hour`> "14:00:00"



    $query = "SELECT of.id AS id_food_order,`orders`.id AS id_order,
              `orders`.hour,`orders`.status,`orders`.delivery_place,`orders`.id_client,
              `orders`.id_delivery_man,`foods`.id as id_food,`foods`.name as food_name,
              `foods`.description,`foods`.gluten_free,`foods`.vegan,`foods`.cost,`foods`.active,
              `foods`.id_food_type,`foods`.id_restaurant,`restaurants`.name AS restaurant_name,
              `restaurants`.active AS restaurant_active,`restaurants`.id_owner
              FROM order_foods AS of
              JOIN orders ON id_order=`orders`.id
              JOIN foods ON id_food=`foods`.id
              JOIN restaurants ON id_restaurant=`restaurants`.id
              WHERE `id_owner` = :idseller AND `foods`.`active`= 1 AND `hour`> :hournow" ;

    $db->beginTransaction();
    $stmt = $db->prepare($query);
    $stmt->bindParam(":idseller", $idseller);
    $stmt->bindParam(":hournow", $hournow);
    $stmt->execute();
    $db->commit();

    $result = $stmt -> fetchAll();

    // var_dump($result[0]);
    // var_dump(!empty($result));

    if (!empty($result)) {
      echo "ok";
    }else {
      echo "notfound";
    }
