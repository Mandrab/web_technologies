<?php

    // TODO : check auth

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/db_connection.php';

    $idseller = $_SESSION["id"];
    if(empty($idresturant)){
      $idrestaurant = $_REQUEST['idrestaurant'];
    }


    $query = "SELECT `foods`.*, `food_types`.description AS tdes, `food_types`.`name` AS tipo  FROM foods JOIN food_types ON `foods`.id_food_type = `food_types`.id
              WHERE `id_restaurant` = :idrestaurant AND `active` = 1 ORDER BY `foods`.id_food_type";

    $db->beginTransaction();
    $stmt = $db->prepare($query);
    $stmt->bindParam(":idrestaurant", $idrestaurant);
    $stmt->execute();
    $db->commit();

    $result = $stmt -> fetchAll();

    // var_dump($result[0]['description']);
    // var_dump($result);
    // die();

    require $_SERVER['DOCUMENT_ROOT'] . '/src/client/logged/seller/menu_mod.php';
