<?php

    // TODO : check auth

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/db_connection.php';

    $idseller = $_SESSION["id"];
    $idrestaurant = $_REQUEST['idrestaurant'];

    $query = "SELECT `restaurants`.*, `users`.name AS owner_name, `users`.`surname`, `restaurant_types`.name AS type
            FROM restaurant_types JOIN (`restaurants` JOIN `users` ON `restaurants`.id_owner = `users`.id) ON `restaurants`.id_restaurant_type = `restaurant_types`.id
            WHERE `restaurants`.id = :idrestaurant AND `restaurants`.id_owner = :idseller" ;

    $db->beginTransaction();
    $stmt = $db->prepare($query);
    $stmt->bindParam(":idseller", $idseller);
    $stmt->bindParam(":idrestaurant", $idrestaurant);
    $stmt->execute();
    $db->commit();

    $result = $stmt -> fetchAll();


    $query2 = "SELECT `id`,`name` FROM `restaurant_types`ORDER BY 'id'" ;

    $db->beginTransaction();
    $stmt = $db->prepare($query2);
    $stmt->execute();
    $db->commit();

    $result2 = $stmt -> fetchAll();


    $query3 = "SELECT `id`, `week_day`, `opening_hour`, `closing_hour`, `id_restaurant` FROM `openings` WHERE `id_restaurant` = :idrestaurant ORDER BY `week_day`";
    $db->beginTransaction();
    $stmt = $db->prepare($query3);
    $stmt->bindParam(":idrestaurant", $idrestaurant);
    $stmt->execute();
    $db->commit();

    $result3 = $stmt -> fetchAll();

    // var_dump($result3);
    // die();



    require $_SERVER['DOCUMENT_ROOT'] . '/src/client/logged/seller/restaurant_mod.php';
