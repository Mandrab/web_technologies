<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/check_permission.php';
    checkAuth( array( 'seller' ) );

    //check auth

    $idseller = $_SESSION["id"];

    if ( $_REQUEST['type'] == "orders" ) {
// TODO
        $query = "SELECT * FROM users";
        if ( isset( $_REQUEST[ 'filter' ] ) && $_REQUEST[ 'filter' ] != 'all' )
            $query = $query . ( $_REQUEST[ 'filter' ] == "active"
                ? " WHERE active" : " WHERE NOT active" );

    } else if ( $_REQUEST['type'] == "foods" ) {
// TODO
        $query = "SELECT * FROM foods";
        if ( isset( $_REQUEST[ 'filter' ] ) && $_REQUEST[ 'filter' ] != 'all' )
            $query = $query . ( $_REQUEST[ 'filter' ] == "active"
                ? " WHERE active" : " WHERE NOT active" );

    } else {

        $query = "SELECT *, photo_link AS photo, id_restaurant AS idr FROM restaurants LEFT JOIN (
            SELECT AVG(val) AS vote, id_restaurant, COUNT(id) AS n_votes
            FROM votes
            GROUP BY id_restaurant ) AS votes
        ON restaurants.id = votes.id_restaurant
        WHERE `restaurants`.active
        AND `restaurants`.id_owner = :id";

    }

    $db->beginTransaction();
    $stmt = $db->prepare($query);
    $stmt->bindParam( ':id', $idseller );
