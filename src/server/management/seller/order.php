<?php

    // TODO : check auth

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/db_connection.php';
    $idorder = $_REQUEST['idorder'];


    $query = "SELECT `id_order`,id_food,name,description,cost,COUNT(*) AS quantity
                FROM `order_foods`
                JOIN `foods` ON  `order_foods`.`id_food`= `foods`.`id`
                WHERE `id_order`= :idorder
                GROUP BY id_food" ;

    $db->beginTransaction();
    $stmt = $db->prepare($query);
    $stmt->bindParam(":idorder", $idorder);
    $stmt->execute();
    $db->commit();

    $result = $stmt -> fetchAll();


    require $_SERVER['DOCUMENT_ROOT'] . '/src/client/logged/seller/order_foods_list.php';
