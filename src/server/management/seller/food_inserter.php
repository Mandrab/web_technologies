<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/db_connection.php';

    $idresturant = $_REQUEST['id_restaurant'];

    $operation = $_REQUEST['db_operation'];
    $foodname = $_REQUEST['food_name'];
    $foodcat = $_REQUEST['food_cat'];
    $foodspecial = $_REQUEST['food_special'];
    $fooddesc = $_REQUEST['desc'];
    $foodcost = $_REQUEST['food_price'];
    $veg=0;
    $noglut=0;

    if($foodspecial == 2 || $foodspecial == 4){
      $veg=1;
    }
    if($foodspecial == 3 || $foodspecial == 4){
      $noglut=1;
    }
    // var_dump($veg);
    // var_dump($noglut);
    if($operation == 'update'){
      $idfood = $_REQUEST['id_food'];
    }
    // var_dump($idresturant);
    // // var_dump($idfood);
    // var_dump($operation);
    // var_dump($foodname);
    // var_dump($foodcat);
    // var_dump($foodspecial);
    // var_dump($fooddesc);
    // var_dump($foodcost);
    // die();

    //TODO controllare che cost sia numeric

    if($operation == 'update'){
        $query = "UPDATE `foods`
                  SET `name`= :name ,`description`=:description,`gluten_free`=:noglut,
                  `vegan`=:vegan,`cost`=:cost,`id_food_type`=:foodcat WHERE `id`= :id_food";

        $db->beginTransaction();
        $stmt = $db->prepare($query);
        $stmt->bindParam(":name", $foodname);
        $stmt->bindParam(":description", $fooddesc);
        $stmt->bindParam(":noglut", $noglut);
        $stmt->bindParam(":vegan", $veg);
        $stmt->bindParam(":cost", $foodcost);
        $stmt->bindParam(":foodcat", $foodcat);
        $stmt->bindParam(":id_food", $idfood);
        $stmt->execute();
        $db->commit();

        echo "okU";

    }else if($operation == 'insert'){
        $query = "INSERT INTO `foods`( `name`, `description`, `gluten_free`, `vegan`, `cost`, `active`, `id_food_type`, `id_restaurant`)
                  VALUES (:name,:description,:noglut,:vegan,:cost,1,:foodcat,:idrestaurant)";

                  $db->beginTransaction();
                  $stmt = $db->prepare($query);
                  $stmt->bindParam(":name", $foodname);
                  $stmt->bindParam(":description", $fooddesc);
                  $stmt->bindParam(":noglut", $noglut);
                  $stmt->bindParam(":vegan", $veg);
                  $stmt->bindParam(":cost", $foodcost);
                  $stmt->bindParam(":foodcat", $foodcat);
                  $stmt->bindParam(":idrestaurant", $idresturant);
                  $stmt->execute();
                  $db->commit();

        echo "okI";

    }else{
        echo "err";
    }
