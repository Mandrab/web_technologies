<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/db_connection.php';

    $idresturant = $_REQUEST['id_restaurant'];
    $opening_hour = $_REQUEST['open'];
    $closing_hour = $_REQUEST['close'];
    $week_day = $_REQUEST['days'];

    $query = "SELECT *
              FROM openings
              WHERE week_day=:week_day AND id_restaurant=:id_restaurant";

    $db->beginTransaction();
    $stmt = $db->prepare($query);
    $stmt->bindParam(":week_day", $week_day);
    $stmt->bindParam(":id_restaurant", $idresturant);
    $stmt->execute();
    $db->commit();

    $result = $stmt -> fetchAll();
    // var_dump(!empty($result));
    if (!empty($result)) {
        //vuol dire che devo fare un UPDATE
        $query = "UPDATE `openings` SET `week_day`=:week_day,  `opening_hour`=:opening_hour, `closing_hour`=:closing_hour, `id_restaurant`=:id_restaurant
                WHERE `week_day`= :week_day AND `id_restaurant`=:id_restaurant";

    }else {
        //vuol dire che devo fare un INSERT
        $query = "INSERT INTO openings(week_day, opening_hour, closing_hour, id_restaurant) VALUES (:week_day, :opening_hour, :closing_hour, :id_restaurant)";
    }
    $db->beginTransaction();
    $stmt = $db->prepare($query);
    $stmt->bindParam(":week_day", $week_day);
    $stmt->bindParam(":opening_hour", $opening_hour);
    $stmt->bindParam(":closing_hour", $closing_hour);
    $stmt->bindParam(":id_restaurant", $idresturant);
    $stmt->execute();
    $db->commit();
