<?php

    // TODO : check auth

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/db_connection.php';

    $idseller = $_SESSION["id"];
    $idfood = $_REQUEST['idfood'];
    $idrestaurant = $_REQUEST['idrestaurant'];

    $query = "SELECT `foods`.*, `food_types`.description AS tdes , `food_types`.`name` AS tipo  FROM foods JOIN food_types ON `foods`.id_food_type = `food_types`.id
              WHERE `foods`.`id` = :idfood AND `active` = 1 ORDER BY tipo";

    $db->beginTransaction();
    $stmt = $db->prepare($query);
    $stmt->bindParam(":idfood", $idfood);
    $stmt->execute();
    $db->commit();
    $result = $stmt -> fetchAll();

    if(!empty($result)){
      $query = "DELETE FROM `foods` WHERE `foods`.`id` = :idfood";
      
      $db->beginTransaction();
      $stmt = $db->prepare($query);
      $stmt->bindParam(":idfood", $idfood);
      $stmt->execute();
      $db->commit();

    }

    // if(!empty($result[0])){
    //   var_dump($idfood);
    //   die();
    // }

    // var_dump($result[0]['description']);
    // var_dump($result);
    // die();

    require $_SERVER['DOCUMENT_ROOT'] . '/src/server/management/seller/menu_mod.php';
    //die();
    //window.location.href = "/src/server/management/seller/menu_mod.php?idrestaurant=". $idresturant."";
