<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/db_connection.php';

    $query = "INSERT INTO restaurants
            VALUES (NULL, :name, :address, :tel, :p_iva, \"/res/img/restaurants/no_image.svg\", 
            :description, 0, :d_time, :id_owner, :types)";

    $db->beginTransaction();
    $stmt = $db->prepare( $query );
    $stmt->bindParam( ':name', $_REQUEST['restaurant_name'] );
    $stmt->bindParam( ':address', $_REQUEST['address'] );
    $stmt->bindParam( ':tel', $_REQUEST['tel'] );
    $stmt->bindParam( ':p_iva', $_REQUEST['iva'] );
    $stmt->bindParam( ':description', $_REQUEST['des'] );
    $stmt->bindParam( ':d_time', $_REQUEST['d_time'] );
    $stmt->bindParam( ':id_owner', $_SESSION['id'] );
    $stmt->bindParam( ':types', $_REQUEST['types'] );
    $stmt->execute();
    $db->commit();

    $query = "SELECT id FROM restaurants ORDER BY id DESC LIMIT 1";

    $db->beginTransaction();
    $stmt = $db->prepare( $query );
    $stmt->execute();
    $db->commit();

    echo $stmt->fetch()['id'];