<?php

    // TODO : check auth

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/db_connection.php';
    $idseller = $_SESSION["id"];


    $query = "SELECT * FROM `restaurants` WHERE 1 ORDER BY id DESC" ;

    $db->beginTransaction();
    $stmt = $db->prepare($query);
    $stmt->execute();
    $db->commit();

    $result = $stmt -> fetchAll();

    //se sono nuovo allora inserisco un ristorante fittizio che poi verrà modificato alla conferma, sarà comunque active=0 quindi se ha valori sballati l'admin può cancellarlo
    if (!isset($_SESSION['new_restaurant'])) {
        $_SESSION['new_restaurant']=$result[0]['id']+1;
        $idrestaurantnew = $_SESSION['new_restaurant'];
        var_dump("sono nuovo");

        $query = "INSERT INTO `restaurants`(`id`, `name`, `address`, `tel`, `p_iva`, `photo_link`, `description`, `active`, `del_predicted_time`, `id_owner`, `id_restaurant_type`)
                    VALUES (:id_restaurantnew,'Nome Ristorante*','Indirizzo Ristorante*','00000000','Partita Iva*',
                    'xxxxxxxxxx','Descrizione e ingredienti*',0,'00:00:00',:id_owner,1)";
        $db->beginTransaction();
        $stmt = $db->prepare($query);
        $stmt->bindParam(":id_owner", $idseller);
        $stmt->bindParam(":id_restaurantnew", $idrestaurantnew);
        $stmt->execute();
        $db->commit();

        $type = 'new restaurant';
        $id_order = '';
        $id_restaurant = $idrestaurantnew;
        $id_reciver = $idseller;

        require $_SERVER['DOCUMENT_ROOT'].'/src/server/management/utils/notifier.php';
    }else {
        $idrestaurantnew = $_SESSION['new_restaurant'];
    }


    // var_dump($idrestaurantnew);
    // //unset($_SESSION['new_restaurant']);
    // die();


    ////////// TODO:  RICORDARSI DI FARE L'UNSET DI $_SESSION["new_restaurant"] alla conferma delle modifiche


    $query = "SELECT `restaurants`.*, `users`.name AS owner_name, `users`.`surname`, `restaurant_types`.name AS type
            FROM restaurant_types JOIN (`restaurants` JOIN `users` ON `restaurants`.id_owner = `users`.id) ON `restaurants`.id_restaurant_type = `restaurant_types`.id
            WHERE `restaurants`.id = :idrestaurant AND `restaurants`.id_owner = :idseller" ;

    $db->beginTransaction();
    $stmt = $db->prepare($query);
    $stmt->bindParam(":idseller", $idseller);
    $stmt->bindParam(":idrestaurant", $idrestaurantnew);
    $stmt->execute();
    $db->commit();

    $result = $stmt -> fetchAll();


    $query2 = "SELECT `id`,`name` FROM `restaurant_types`ORDER BY 'id'" ;

    $db->beginTransaction();
    $stmt = $db->prepare($query2);
    $stmt->execute();
    $db->commit();

    $result2 = $stmt -> fetchAll();


    $query3 = "SELECT `id`, `week_day`, `opening_hour`, `closing_hour`, `id_restaurant` FROM `openings` WHERE `id_restaurant` = :idrestaurant ORDER BY `week_day`";
    $db->beginTransaction();
    $stmt = $db->prepare($query3);
    $stmt->bindParam(":idrestaurant", $idrestaurantnew);
    $stmt->execute();
    $db->commit();

    $result3 = $stmt -> fetchAll();

    $idrestaurant = $idrestaurantnew;
    // var_dump($result3);
    // die();



    require $_SERVER['DOCUMENT_ROOT'] . '/src/client/logged/seller/restaurant_mod.php';
