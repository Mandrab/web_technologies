<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/check_permission.php';
    checkAuth( array( 'seller' ) );

    require $_SERVER['DOCUMENT_ROOT'] . '/src/client/logged/seller/home.php';