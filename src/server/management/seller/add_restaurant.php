<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/db_connection.php';

    $query = "SELECT `id`,`name` FROM `restaurant_types` ORDER BY 'id'" ;

    $db->beginTransaction();
    $stmt = $db->prepare( $query );
    $stmt->execute();
    $db->commit();

    $result = $stmt -> fetchAll();

    require $_SERVER['DOCUMENT_ROOT'] . '/src/client/logged/seller/add_restaurant.php';