<?php
    // TODO : check auth
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/db_connection.php';

    $day = isset($_REQUEST['day']) ? $_REQUEST['day'] : false;
    $hour = isset($_REQUEST['hour']) ? $_REQUEST['hour'] : false;
    $name = isset($_REQUEST['name']) ? $_REQUEST['name'] : false;
    $type = isset($_REQUEST['type']) ? $_REQUEST['type'] : false;

    $restaurants = "SELECT *, photo_link AS photo, restaurants.id 
                FROM restaurants LEFT JOIN (SELECT AVG(val) AS vote, id_restaurant, COUNT(id) AS n_votes
                                            FROM votes
                                            GROUP BY id_restaurant ) AS votes
                                            ON restaurants.id = votes.id_restaurant
                                            LEFT JOIN openings ON restaurants.id = openings.id_restaurant WHERE 1 ";

    if ($day)
        $restaurants.=" AND openings.week_day = :day";
    if ($hour)
        $restaurants.=" AND openings.opening_hour <= :hour AND openings.closing_hour >= :hour ";
    if ($name)
        $restaurants.=" AND LOWER(restaurants.name) LIKE LOWER(CONCAT('%', :name, '%')) ";
    if ($type)    
        $restaurants.=" AND restaurants.id_restaurant_type = :type ";

    $restaurants.="GROUP BY restaurants.id";

    $stmt = $db->prepare($restaurants);
    if($name)
        $stmt->bindParam(":name", $name);
    if($day)
        $stmt->bindParam(":day", $day);
    if($hour)
        $stmt->bindParam(":hour", $hour);
    if($type)
        $stmt->bindParam(":type", $type);
    $stmt->execute();

    $result = $stmt -> fetchAll();

    $cat = "SELECT * FROM restaurant_types";
    $stmt = $db->prepare($cat);
    $stmt->execute();
    $cat = $stmt -> fetchAll();


    require $_SERVER['DOCUMENT_ROOT'] . '/src/client/logged/user/all_restaurant.php';

    function ceiling($number, $significance = 1)
    {
        return ( is_numeric($number) && is_numeric($significance) ) ? (ceil($number/$significance)*$significance) : false;
    }