<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/db_connection.php';

    if(isset($_COOKIE['cart'])){
        $array = json_decode($_COOKIE['cart'], true);
        $id_r = $array[0];

        $res = "SELECT * FROM restaurants WHERE restaurants.id=:id";
        $prod = "SELECT * FROM foods WHERE foods.id=:id";
        $openings = "SELECT * FROM openings WHERE openings.id_restaurant = :id AND openings.week_day = :date";

        $stmt = $db->prepare($res);
        $stmt->bindParam(":id", $id_r);
        $stmt->execute();
        $res = $stmt -> fetchAll();

        $day = date('N');
        $stmt = $db->prepare($openings);
        $stmt->bindParam(":id", $id_r);
        $stmt->bindParam(":date", $day);
        $stmt->execute();
        $openings = $stmt -> fetchAll();
        $valid_hour=[];
        foreach( $openings as $op ){
            $oh = explode(":",$op['opening_hour'])[0];
            $ch = explode(":",$op['closing_hour'])[0];
            $i = $ch - $oh;
            for($k = 0; $k<$i; $k++){
                $tmp = $oh+$k;
                $valid_hour[] = $tmp.":00:00";
                $valid_hour[] = $tmp.":30:00";
            }            
        }

        $stmt = $db->prepare($prod);
        $array_prod = [];
        foreach($array[1] as $key=>$p){
            $stmt->bindParam(":id", $key);
            $stmt->execute();
            $tmp = $stmt -> fetchAll();
            $tmp["qt"] = $array[1][$key];
            $array_prod[] = $tmp;
        }
    }

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/logged/user/see_cart.php';