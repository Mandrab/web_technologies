<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/check_permission.php';
    checkAuth( array( 'user' ) );

    if ( $_REQUEST['type'] == "order" ) {
        $query = "SELECT o.id, o.hour, o.status, o.delivery_place, r.name, SUM(f.cost) AS cost
                FROM orders AS o JOIN order_foods AS of 
                ON o.id = of.id_order JOIN foods AS f 
                ON of.id_food = f.id JOIN restaurants AS r
                ON r.id = f.id_restaurant
                WHERE id_client = :id
                GROUP BY o.id";
    }

    $db->beginTransaction();
    $stmt = $db->prepare( $query );
    $stmt->bindParam( ':id', $_SESSION['id'] );