<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/check_permission.php';
    checkAuth( array( 'user' ) );

    require $_SERVER['DOCUMENT_ROOT'] . '/src/client/logged/user/home.php';