<?php
    // TODO : check auth
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/db_connection.php';

    $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : false;

    $restaurant = "SELECT *, photo_link AS photo 
                FROM restaurants LEFT JOIN (SELECT AVG(val) AS vote, id_restaurant, COUNT(id) AS n_votes
                                            FROM votes
                                            GROUP BY id_restaurant ) AS votes
                                            ON restaurants.id = votes.id_restaurant
                                            WHERE restaurants.id = :id";
    $openings = "SELECT * FROM openings WHERE openings.id_restaurant=:id";
    $foods = "SELECT *, food_types.name AS categories
            FROM foods INNER JOIN food_types ON food_types.id = foods.id_food_type
            WHERE foods.id_restaurant=:id";

    $stmt = $db->prepare($restaurant);
    $stmt->bindParam(":id", $id);
    $stmt->execute();

    $restaurant = $stmt -> fetchAll();

    $stmt = $db->prepare($openings);
    $stmt->bindParam(":id", $id);
    $stmt->execute();

    $openings = $stmt -> fetchAll();

    $stmt = $db->prepare($foods);
    $stmt->bindParam(":id", $id);
    $stmt->execute();

    $foods = $stmt -> fetchAll();

    foreach($openings as $op){
        $restaurant['hours'][] = $op['week_day'].'|'.$op['opening_hour']."|".$op['closing_hour']; 
    }

    $cat = [];
    foreach($foods as $food){
        if(!in_array($food['categories'],$cat)){
            $cat[ $food['categories']] = [];
        }
    }
    foreach($cat as $key=>$c){
        foreach($foods as $food){
            if($food['categories'] == $key){
                $cat[$key][]=$food;
            }
        }
    }

    require $_SERVER['DOCUMENT_ROOT'] . '/src/client/logged/user/single_restaurant.php';

    function ceiling($number, $significance = 1)
    {
        return ( is_numeric($number) && is_numeric($significance) ) ? (ceil($number/$significance)*$significance) : false;
    }