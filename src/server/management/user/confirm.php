<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/db_connection.php';

    if(isset($_COOKIE['cart'])){
        $array = json_decode($_COOKIE['cart'], true);
        $hour = $_REQUEST['hour'];
        $id = $_SESSION["id"];

        $res = "INSERT INTO orders(hour, status, delivery_place, id_client, id_delivery_man) 
                VALUES (:hour,'New','Segreteria Università',:id_client,1)";

        $db->beginTransaction();
        $stmt = $db->prepare($res);
        $stmt->bindParam(":hour", $hour);
        $stmt->bindParam(":id_client", $id);
        $stmt->execute();
        $db->commit();
        $id_last = $db->lastInsertId();

        $res = "INSERT INTO `order_foods`(`id_food`, `id_order`) 
                VALUES (:id_food,:id_order)";

        foreach($array[1] as $key=>$value){
            for($i = 0; $i<$value; $i++){
                $db->beginTransaction();
                $stmt = $db->prepare($res);
                $stmt->bindParam(":id_order", $id_last);
                $stmt->bindParam(":id_food", $key);
                $stmt->execute();
                $db->commit();
            }
        }
    }

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/client/logged/user/see_cart.php';