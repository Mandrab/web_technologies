<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/db_connection.php';

    $id_p = isset($_REQUEST['id_p']) ? $_REQUEST['id_p'] : false;
    $id_r = isset($_REQUEST['id_r']) ? $_REQUEST['id_r'] : false;

    $value = [];
    $value[0] =  $id_r;
    $value[1] = [
        $id_p => 1,
    ];

    if(!isset($_COOKIE['cart'])){
        setcookie('cart', json_encode($value));
    }else{
        $new_array;
        $array = json_decode($_COOKIE['cart'], true);
        $new_array = $array[1];
        if($id_r != $array[0])
            die('err: rest');

        if(in_array($id_p,$array[1])){
            $new_array[$id_p]++;
        }else{
            $new_array[$id_p] = 1;
        }
        $array[1] = $new_array;
        setcookie('cart', json_encode($array));
    }