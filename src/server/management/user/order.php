<?php

    // TODO : check auth

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/db_connection.php';
    $id = $_REQUEST['id'];

    $query = "SELECT id_order,id_food,name,description,cost,COUNT(*) AS quantity
            FROM order_foods
            JOIN foods ON  order_foods.id_food=foods.id
            WHERE id_order=:id
            GROUP BY id_food" ;

    $db->beginTransaction();
    $stmt = $db->prepare($query);
    $stmt->bindParam(":id", $id);
    $stmt->execute();
    $db->commit();

    $result = $stmt -> fetchAll();

    require $_SERVER['DOCUMENT_ROOT'] . '/src/client/logged/commons/order.php';
