<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/db_connection.php';

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/check_permission.php';
    checkAuth( array( 'admin' ) );

    $id = $_REQUEST['id'];
    $val = ( $_REQUEST['operation'] == 'active' || $_REQUEST['operation'] == 'accept' );

    if ( $_REQUEST['type'] == 'restaurant' ) {

        // blocco ristorante
        $query = 'UPDATE restaurants SET active=:val WHERE id=:id';

        $db->beginTransaction();
        $stmt = $db->prepare( $query );
        $stmt->bindParam( ':id', $id );
        $stmt->bindParam( ':val', $val );
        $stmt->execute();
        $db->commit();

        // cerco i dati del rist bloccato
        $query = 'SELECT * FROM restaurants WHERE id=:id';

        $db->beginTransaction();
        $stmt = $db->prepare( $query );
        $stmt->bindParam( ':id', $id );
        $stmt->execute();
        $db->commit();

        $row = $stmt->fetch();

        if ( $_REQUEST['operation'] == 'active' )
            $type = 'restaurant unlocked';
        else if ( $_REQUEST['operation'] == 'accept' )
            $type = 'restaurant accepted';
        else if ( $_REQUEST['operation'] == 'deactive' )
            $type = 'restaurant locked';
        else if ( $_REQUEST['operation'] == 'reject' )
            $type = 'restaurant rejected';
        $id_order = '';
        $id_restaurant = $row['id'];
        $id_receiver = $row['id_owner'];

        //invio una notifica
        require $_SERVER['DOCUMENT_ROOT'] . '/src/server/management/utils/notifier.php';

    } else if ( $_REQUEST['type'] == 'user' ) {

        // blocco l'utente
        $query = 'UPDATE users SET active=:val WHERE id=:id';

        $db->beginTransaction();
        $stmt = $db->prepare( $query );
        $stmt->bindParam( ':id', $id );
        $stmt->bindParam( ':val', $val );
        $stmt->execute();
        $db->commit();

        if ( !$val ) {          // is a lock op

            // metto una notifica di blocco per utente ad ogni ristorante attivo
            $query = 'INSERT INTO notifications 
                    SELECT 0, \':date\', \'locked user\', 0, NULL, id, 8, :id FROM restaurants
                    WHERE id_owner=:id
                    AND active=1';

            $db->beginTransaction();
            $stmt = $db->prepare( $query );
            $stmt->bindParam( ':id', $id );
            $date_ = date("Y-m-d");
            $stmt->bindParam( ':date', $date_ );
            $stmt->execute();
            $db->commit();

            // blocco i ristoranti
            $query = 'UPDATE restaurants SET active=0
                WHERE id_owner=:id';

            $db->beginTransaction();
            $stmt = $db->prepare( $query );
            $stmt->bindParam( ':id', $id );
            $stmt->execute();
            $db->commit();
            
        } else {

            // attivo i ristoranti bloccati al blocco
            $query = 'UPDATE restaurants AS r SET r.active=1
                    WHERE r.id_owner=:id
                    AND r.id IN (
                        SELECT id_restaurant FROM notifications
                        WHERE id_notification_type=8
                        GROUP BY id_restaurant
                        ORDER BY data
                    )';

            $db->beginTransaction();
            $stmt = $db->prepare( $query );
            $stmt->bindParam( ':id', $id );
            $stmt->execute();
            $db->commit();

        }

    } else

        echo 'not';
