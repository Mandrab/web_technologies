<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/check_permission.php';
    checkAuth( array( 'admin' ) );

    require $_SERVER['DOCUMENT_ROOT'] . '/src/client/logged/admin/home.php';