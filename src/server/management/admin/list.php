<?php

    require_once $_SERVER['DOCUMENT_ROOT'] . '/src/server/access/check_permission.php';
    checkAuth( array( 'admin' ) );

    //check auth

    if ( $_REQUEST['type'] == "users" ) {

        $query = "SELECT * FROM users";
        if ( isset( $_REQUEST[ 'filter' ] ) && $_REQUEST[ 'filter' ] != 'all' )
            $query = $query . ( $_REQUEST[ 'filter' ] == "active" 
                ? " WHERE active" : " WHERE NOT active" );
                
    } else if ( $_REQUEST['type'] == "foods" ) {

        $query = "SELECT * FROM foods";
        if ( isset( $_REQUEST[ 'filter' ] ) && $_REQUEST[ 'filter' ] != 'all' )
            $query = $query . ( $_REQUEST[ 'filter' ] == "active" 
                ? " WHERE active" : " WHERE NOT active" );

    } else {

        $query = "SELECT restaurants.*, n_votes, vote, photo_link AS photo, users.name AS owner_name FROM restaurants LEFT JOIN (
                    SELECT AVG(val) AS vote, id_restaurant, COUNT(id) AS n_votes
                    FROM votes
                    GROUP BY id_restaurant ) AS votes
                ON restaurants.id = votes.id_restaurant
                JOIN users ON restaurants.id_owner = users.id";

        if ( isset( $_REQUEST[ 'filter' ] ) && $_REQUEST[ 'filter' ] != 'all' )
            $query = $query . ( $_REQUEST[ 'filter' ] == "active" 
                ? " WHERE restaurants.active" 
                : " WHERE NOT restaurants.active" );

    }

    $db->beginTransaction();
    $stmt = $db->prepare($query);